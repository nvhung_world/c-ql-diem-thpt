﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qldBLL
{
    public class QueryDelete : Query
    {
        string from, where;
        public QueryDelete()
        {
            from = where = "";
        }

        public void Clear(EnumQuery query = EnumQuery.Null)
        {
            switch(query)
            {
                case EnumQuery.from:
                    from = "";
                    break;
                case EnumQuery.where:
                    where = "";
                    break;
                case EnumQuery.Null:
                    from = where = "";
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh delete");
            }
        }
        public void AddString(EnumQuery query, string var)
        {
            switch (query)
            {
                case EnumQuery.from:
                    from = var;
                    break;
                case EnumQuery.where:
                    if (where == "")
                        where = var;
                    else
                        where += " and " + var;
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh delete");
            }
            
        }
        public string GetQuery()
        {
            if (from == "")
                throw new Exception("table khong được bỏ trống");
            if (where == "")
                throw new Exception("danh sách where trong lệnh delete from " + from + " không được bỏ trống");
            string query = "delete from " + from + (where == "" ? "" : " where " + where);
            return query;
        }
    }
}
