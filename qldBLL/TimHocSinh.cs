﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using qldDAL;
namespace qldBLL
{
    public class TimHocSinh
    {
        Table data;
        public DataTable table
        {
            get
            {
                return data.table;
            }
        }
        public TimHocSinh(Session session, string coLumn = "HocSinh.*")
        {
            data = new Table(session);
            data.select.Add(new QuerySelect());
            data.select[0].AddString(EnumQuery.select, coLumn);
            data.select[0].AddString(EnumQuery.from, "HocSinh");
        }

        public string Search(int top = 50,
            string MaHS = "", 
            string HoTen = "",
            string E_Box = "",
            string DiaChi = "",
            string SoDT = "",
            string NgaySinh = "  -  -",
            string KhoaHoc = "",
            bool Nam = false,
            bool Nu = false,
            string Lop = "",
            string NamHoc = "",
            string IDLop = "")
        {
            data.select[0].Clear(EnumQuery.where);

            data.select[0].AddString(EnumQuery.top, top.ToString());

            if (MaHS != "")
                data.select[0].AddString(EnumQuery.where, "HocSinh.MaHocSinh = '" + MaHS + "'");
            if (HoTen != "")
                data.select[0].AddString(EnumQuery.where, "UPPER(concat(HocSinh.HoDem, SPACE(1), HocSinh.Ten)) LIKE  concat('%', UPPER('" + HoTen + "'), '%') ");
            if (E_Box != "")
                data.select[0].AddString(EnumQuery.where, "HocSinh.Email = '" + E_Box + "'");
            if (DiaChi != "")
                data.select[0].AddString(EnumQuery.where, "HocSinh.DiaChi = '" + DiaChi + "'");
            if (SoDT != "")
                data.select[0].AddString(EnumQuery.where, "HocSinh.SoDT = '" + SoDT + "'");
            if (NgaySinh != "  -  -")
                data.select[0].AddString(EnumQuery.where, "HocSinh.NgaySinh = '" + NgaySinh + "'");
            if (KhoaHoc != "")
                data.select[0].AddString(EnumQuery.where, "HocSinh.KhoaHoc = " + KhoaHoc);
            if (Nam || Nu)
                data.select[0].AddString(EnumQuery.where, "HocSinh.GioiTinh = " + (Nam ? 1 : 0));
            if(Lop != "" || IDLop != "" || NamHoc != "")
            {
                data.select[0].Clear(EnumQuery.from);
                data.select[0].AddString(EnumQuery.from, "HocSinh");
                data.select[0].AddString(EnumQuery.from, "PhanLop");
                data.select[0].AddString(EnumQuery.from, "Lop");

                data.select[0].AddString(EnumQuery.where, "Lop.IDLop = PhanLop.IDLop");
                data.select[0].AddString(EnumQuery.where, "PhanLop.MaHocSinh = HocSinh.MaHocSinh");
                if(Lop != "")
                    data.select[0].AddString(EnumQuery.where, "Lop.TenLop = '" + Lop + "'");
                if(IDLop != "")
                    data.select[0].AddString(EnumQuery.where, "Lop.IDLop = " + IDLop + "");
                if (NamHoc != "")
                    data.select[0].AddString(EnumQuery.where, "Lop.NamHoc = " + Nam );
            }
            Console.WriteLine(data.select[0].GetQuery());
            data.SetCommand(Statement.select);
            return data.ReadTable("GiaoVien");
        }
        public string Update()
        {
            return data.Update();
        }

        public void SetupInsert()
        {
            
        }

        public void SetupDelete()
        {

        }
        public void SetupUpdate()
        {

        }
    }
}
