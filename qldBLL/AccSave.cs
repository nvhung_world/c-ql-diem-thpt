﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace QL_Diem
{
    public static class AccSeve
    {
        public static bool SaveAcc = false;
        public static List<Dictionary<string, string>> list;
        public static void Read()
        {
            list = new List<Dictionary<string, string>>();
            StreamReader sr ;
            try
            {
                sr = new StreamReader(File.Open(".\\accsave.as", FileMode.OpenOrCreate));
            }
            catch(Exception)
            {
                return;
            }
            string j = sr.ReadToEnd();
            sr.Close();
            if(j != "")
            {
                Dictionary<string, object> data = (Dictionary<string, object>)Json.Deserialize(j);
                SaveAcc = data["SaveAcc"].ToString() == "True" ? true : false;
                j = Json.Serialize(data["array"]);
                List<object> l = (List<object>)Json.Deserialize(j);
                foreach(object o in l)
                {
                    Dictionary<string, object> d = (Dictionary<string, object>)Json.Deserialize(Json.Serialize(o));
                    Dictionary<string, string> a = new Dictionary<string, string>();
                    a.Add("acc", d["acc"].ToString());
                    a.Add("pass", d["pass"].ToString());
                    list.Add(a);
                }
            }
        }

        public static void Add(string acc, string pass)
        {
            bool b = true;
            for (int i = 0; i < list.Count; i++ )
            {
                if (list[i]["acc"] == acc)
                {
                    b = false;
                    list[i]["pass"] = pass;
                    break;
                }
                Dictionary<string, string> c = list[0];
                list[0] = list[i];
                list[i] = c;
            }
            if(b)
            {
                Dictionary<string, string> d = new Dictionary<string,string>();
                d.Add("acc", acc);
                d.Add("pass",pass);
                list.Insert(0, d); 
            }
            Save();
        }

        public static void Save()
        {
            StreamWriter sw;
            try
            {
                sw = new StreamWriter(File.Open(".\\accsave.as", FileMode.Create));
            }
            catch(Exception)
            {
                return;
            }
            sw.WriteLine("{\n\t\"SaveAcc\": \"" + SaveAcc.ToString() + "\",");
            sw.WriteLine("\t\"array\": [");
            int i = 0;
            foreach (Dictionary<string, string> d in list)
            {
                
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\t\"acc\" : \"" + d["acc"] + "\",");
                sw.WriteLine("\t\t\t\"pass\" : \"" + d["pass"] + "\"");
                sw.Write("\t\t}");
                if(i<list.Count - 1)
                    sw.WriteLine(",");
                else
                    sw.WriteLine("");
                i++;
            }
            sw.WriteLine("\t]\n}");
            sw.Flush();
            sw.Close();
        }
    }
}
