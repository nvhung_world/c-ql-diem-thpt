﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using qldDAL;
using System.Data;
using System.Data.SqlClient;
namespace qldBLL
{
    public class TimGiaoVien
    {
        Table data;
        public DataTable table
        {
            get
            {
                return data.table;
            }
        }
        public TimGiaoVien(Session session, string column = "*")
        {
            data = new Table(session);

            //Tạo select command
            data.select.Add(new QuerySelect());
            data.select[0].AddString(EnumQuery.select, column);
            data.select[0].AddString(EnumQuery.from, "GiaoVien");

        }
        public string Search(int top = 50,
            string MaGV = "",
            string HoTen = "",
            string BoMon = "",
            string SoCMT = "",
            string NgaySinh = "  -  -",
            string SoDT = "",
            string DiaChi = "", 
            string Email = "", 
            bool Nam = false, 
            bool Nu = false,
            string Lop_GiangDay = "",
            string Lop_CN = "")
        {
            data.select[0].Clear(EnumQuery.where);

            data.select[0].AddString(EnumQuery.top, top.ToString());

            if (MaGV != "")
                data.select[0].AddString(EnumQuery.where, "MaGiaoVien = '" + MaGV + "'");
            if (HoTen != "")
                data.select[0].AddString(EnumQuery.where, "UPPER(concat(HoDem, SPACE(1), Ten)) LIKE  concat('%', UPPER('" + HoTen + "'), '%') ");
            if (BoMon != "")
                data.select[0].AddString(EnumQuery.where, "BoMon = '" + BoMon + "'");
            if (SoCMT != "")
                data.select[0].AddString(EnumQuery.where, "SoCMT = '" + SoCMT + "'");
            if (NgaySinh != "  -  -")
                data.select[0].AddString(EnumQuery.where, "NgaySinh = '" + NgaySinh + "'");
            if (SoDT != "")
                data.select[0].AddString(EnumQuery.where, "SDT = '" + SoDT + "'");
            if (DiaChi != "")
                data.select[0].AddString(EnumQuery.where, "DiaChi = '" + DiaChi + "'");
            if (Email != "")
                data.select[0].AddString(EnumQuery.where, "Email = '" + Email + "'");
            if (Nam || Nu)
                data.select[0].AddString(EnumQuery.where, "GioiTinh = " + (Nam ? 1 : 0));
            if(Lop_CN != "" || Lop_GiangDay != "")
            {
                data.select[0].Clear(EnumQuery.from);
                data.select[0].AddString(EnumQuery.from, "GiaoVien,Lop");
                if(Lop_CN != "")
                    data.select[0].AddString(EnumQuery.where, "Lop.GiaoVienChuNhiem = GiaoVien.MaGiaoVien and Lop.IDLop = " + Lop_CN);
                if (Lop_GiangDay != "")
                {
                    data.select[0].AddString(EnumQuery.from, "PhanCongGV");
                    data.select[0].AddString(EnumQuery.where, "PhanCongGV.MaGiaoVien = GiaoVien.MaGiaoVien and PhanCongGV.IDLop = " + Lop_GiangDay);
                }
            }
            data.SetCommand(Statement.select);

            return data.ReadTable("GiaoVien");
        }
        public string Update()
        {
            return data.Update();
        }

        public void SetupInsert()
        {
            //Tạo Insert command
            data.insert.Add(new QueryInsert());
            data.insert[0].AddString(EnumQuery.from, "TaiKhoan");
            data.insert[0].AddString(EnumQuery.column, "MaGiaoVien, MatKhau");
            data.insert[0].AddString(EnumQuery.value, "@MaGiaoVien, 'admin'");
            data.insert.Add(new QueryInsert());
            data.insert[1].AddString(EnumQuery.from, "GiaoVien");
            data.insert[1].AddString(EnumQuery.value, "@MaGiaoVien, @BoMon, @HoDem, @Ten, @SoCMT, @NgaySinh, @GioiTinh, @NgayVaoTruong, @NgayRoiTruong, @DiaChi, @Email, @SDT");
            data.SetCommand(Statement.insert);

            data.AddValue(Statement.insert, new SqlParameter("@MaGiaoVien", SqlDbType.NVarChar, 10, "MaGiaoVien"));
            data.AddValue(Statement.insert, new SqlParameter("@BoMon", SqlDbType.NVarChar, 20, "BoMon"));
            data.AddValue(Statement.insert, new SqlParameter("@HoDem", SqlDbType.NVarChar, 40, "HoDem"));
            data.AddValue(Statement.insert, new SqlParameter("@Ten", SqlDbType.NVarChar, 10, "Ten"));
            data.AddValue(Statement.insert, new SqlParameter("@SoCMT", SqlDbType.VarChar, 15, "SoCMT"));
            data.AddValue(Statement.insert, new SqlParameter("@NgaySinh", SqlDbType.Date, 20, "NgaySinh"));
            data.AddValue(Statement.insert, new SqlParameter("@GioiTinh", SqlDbType.Bit, 1, "GioiTinh"));
            data.AddValue(Statement.insert, new SqlParameter("@NgayVaoTruong", SqlDbType.Date, 20, "NgayVaoTruong"));
            data.AddValue(Statement.insert, new SqlParameter("@NgayRoiTruong", SqlDbType.Date, 20, "NgayRoiTruong"));
            data.AddValue(Statement.insert, new SqlParameter("@DiaChi", SqlDbType.NVarChar, 60, "DiaChi"));
            data.AddValue(Statement.insert, new SqlParameter("@Email", SqlDbType.NVarChar, 40, "Email"));
            data.AddValue(Statement.insert, new SqlParameter("@SDT", SqlDbType.NVarChar, 11, "SDT"));
        }

        public void SetupDelete()
        {
            //Tạo Delete command
            data.delete.Add(new QueryDelete());
            data.delete[0].AddString(EnumQuery.from, "GiaoVien");
            data.delete[0].AddString(EnumQuery.where, "MaGiaoVien = @MaGV");
            data.delete.Add(new QueryDelete());
            data.delete[1].AddString(EnumQuery.from, "TaiKhoan");
            data.delete[1].AddString(EnumQuery.where, "MaGiaoVien = @MaGV");
            data.SetCommand(Statement.delete);

            data.AddValue(Statement.delete, new SqlParameter("@MaGV", SqlDbType.NVarChar, 10, "MaGiaoVien"));
        }

        public void SetupUpdate()
        {

        }
    }
}
