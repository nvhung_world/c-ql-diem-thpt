﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using qldDAL;
using System.Data;
namespace qldBLL
{
    public class TimLop
    {
        Table data;
        public DataTable table
        {
            get
            {
                return data.table;
            }
        }
        public TimLop(Session sesion, string column = "Lop.*")
        {
            data = new Table(sesion);
            data.select.Add(new QuerySelect());
            data.select[0].AddString(EnumQuery.select, column);
            data.select[0].AddString(EnumQuery.from, "Lop");
        }
        public string Search(int top = 50, 
            string IDLop = "", 
            string TenLop = "", 
            string LHDT_Box = "", 
            string Lop = "", 
            string NamHoc = "", 
            string GV_ChuNhiem = "", 
            string LopTruong = "",
            bool LaythongTin_CN = false)
        {
            data.select[0].Clear(EnumQuery.where);
            data.select[0].AddString(EnumQuery.top, top.ToString());

            if (IDLop != "")
            {
                data.select[0].AddString(EnumQuery.where, "Lop.IDLop = " + IDLop);
                if (LaythongTin_CN)
                {
                    data.select[0].Clear(EnumQuery.from);
                    data.select[0].AddString(EnumQuery.from, "GiaoVien, Lop");
                    data.select[0].AddString(EnumQuery.where, "Lop.GiaoVienChuNhiem = GiaoVien.MaGiaoVien");
                }
            }
            if (TenLop != "")
                data.select[0].AddString(EnumQuery.where, "Lop.TenLop = '" + TenLop + "'");
            if (LHDT_Box != "")
                data.select[0].AddString(EnumQuery.where, "Lop.LoaiHinhDaoTao = '" + LHDT_Box + "'");
            if (Lop != "")
                data.select[0].AddString(EnumQuery.where, "Lop.Lop = " + Lop);
            if (NamHoc != "")
                data.select[0].AddString(EnumQuery.where, "Lop.NamHoc = " + NamHoc);
            if (GV_ChuNhiem != "")
            {
                data.select[0].AddString(EnumQuery.where, "Lop.GiaoVienChuNhiem = '" + GV_ChuNhiem + "'");
            }
            if (LopTruong != "")
                data.select[0].AddString(EnumQuery.where, "Lop.LopTruong = '" + LopTruong + "'");

            data.SetCommand(Statement.select);

            return data.ReadTable("Lop");
        }
        public string Update()
        {
            return data.Update();
        }

        public void SetupInsert()
        {

        }

        public void SetupDelete()
        {

        }
        public void SetupUpdate()
        {

        }
    }
}
