﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using qldDAL;
using System.Data;
using System.Data.SqlClient;

namespace qldBLL
{
    public class PhanCongGV
    {
        Table data;
        public DataTable table
        {
            get
            {
                return data.table;
            }
        }
        string idLop;
        public PhanCongGV(Session session, string idLop)
        {
            this.idLop = idLop;
            data = new Table(session);
            SetupInsert();
            SetupDelete();
            //Tạo select command
            data.select.Add(new QuerySelect());
            data.select[0].AddString(EnumQuery.select, "GiaoVien.MaGiaoVien, concat(GiaoVien.HoDem, SPACE(1), GiaoVien.Ten) as 'HoTen', GiaoVien.BoMon");
            data.select[0].AddString(EnumQuery.from, "GiaoVien, PhanCongGV");
        }

        public string Search()
        {
            data.select[0].Clear(EnumQuery.where);
            data.select[0].AddString(EnumQuery.where, "GiaoVien.MaGiaoVien = PhanCongGV.MaGiaoVien");
            data.select[0].AddString(EnumQuery.where, "PhanCongGV.IDLop = " + idLop);

            data.SetCommand(Statement.select);

            return data.ReadTable("GiaoVien");
        }

        public string Update()
        {
            return data.Update();
        }
        public void SetupInsert()
        {
            data.insert.Add(new QueryInsert());
            data.insert[0].AddString(EnumQuery.from, "PhanCongGV");
            data.insert[0].AddString(EnumQuery.column, "IDLop,MaGiaoVien");
            data.insert[0].AddString(EnumQuery.value, "@IDLopInsert,@MaGiaoVien");
            data.SetCommand(Statement.insert);

            data.AddValue(Statement.insert, new SqlParameter("@IDLopInsert", idLop));
            data.AddValue(Statement.insert, new SqlParameter("@MaGiaoVien", SqlDbType.VarChar, 10, "MaGiaoVien"));
        }

        public void SetupDelete()
        {
            data.delete.Add(new QueryDelete());
            data.delete[0].AddString(EnumQuery.from, "PhanCongGV");
            data.delete[0].AddString(EnumQuery.where, "IDLop = @IDLopDelete and MaGiaoVien = @MaGV");
            data.SetCommand(Statement.delete);
            data.AddValue(Statement.delete, new SqlParameter("@IDLopDelete", idLop));
            data.AddValue(Statement.delete, new SqlParameter("@MaGV", SqlDbType.VarChar, 10, "MaGiaoVien"));
        }

        public void SetupUpdate()
        {

        }
    }
}
