﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using qldDAL;
namespace qldBLL
{
    public class NhapDiem
    {
        string MaGV;
        int hocKi;
        Table Lop, bangDiem;
        TimLop timLop;
        public DataTable table
        {
            get
            {
                return bangDiem.table;
            }
        }
        public Dictionary<int, Dictionary<string,string>> List;
        public NhapDiem(Session session, int hocKi, int namHoc)
        {
            MaGV = session.acc.maGV;
            this.hocKi = hocKi;
            
            Lop = new Table(session);
            //Tạo select danh sách lớp giảng dậy
            Lop.select.Add(new QuerySelect());
            Lop.select[0].AddString(EnumQuery.select, "Lop.Lop, Lop.TenLop, Lop.IDLop");
            Lop.select[0].AddString(EnumQuery.from, "Lop,PhanCongGV");
            Lop.select[0].AddString(EnumQuery.where, "PhanCongGV.MaGiaoVien = '" + MaGV + "'");
            Lop.select[0].AddString(EnumQuery.where, "Lop.IDLop = PhanCongGV.IDLop"); 
            Lop.select[0].AddString(EnumQuery.where, "Lop.NamHoc = " + namHoc);
            Lop.SetCommand(Statement.select);
            //Tạo select lấy thông tin lớp Lớp
            timLop = new TimLop(session, "concat(GiaoVien.HoDem, SPACE(1), GiaoVien.Ten) as 'ChuNhiem', Lop.LoaiHinhDaoTao");
            
            bangDiem = new Table(session);
            //Tạo select nấy bảng điểm
            bangDiem.data.SetCommand(CommandName.select, "BangDiem_GV", CommandType.StoredProcedure);
            
            //Tạo Update bảng điểm
            bangDiem.data.SetCommand(CommandName.update, "NhapDiem",CommandType.StoredProcedure);
        }
        public string Init()
        {
            List = new Dictionary<int, Dictionary<string, string>>();
            List.Add(0,new Dictionary<string,string>());
            List.Add(1, new Dictionary<string, string>());
            List.Add(2, new Dictionary<string, string>());
            string s = Lop.ReadTable("Lop");
            foreach (DataRow dr in Lop.table.Rows)
                List[Convert.ToInt16(dr[0]) - 10].Add(dr[1].ToString(), dr[2].ToString());
            return s;
        }
        public List<string> ReadTable(int Khoi, string TenLop)
        {
            List<string> s = new List<string>();
            Dictionary<string,string> k;
            if(List.TryGetValue(Khoi,out k))
            {
                string IDLop;
                if(k.TryGetValue(TenLop, out IDLop))
                {
                    // Nấy Thông tin lớp Học
                    s.Add(timLop.Search(IDLop: IDLop, LaythongTin_CN: true));
                    s.Add(timLop.table.Rows[0][0].ToString());
                    s.Add(timLop.table.Rows[0][1].ToString());
                    //Nấy Bảng điểm
                    bangDiem.data.ClearValue(CommandName.select);
                    bangDiem.data.AddValue(CommandName.select, new SqlParameter("@IDLop", IDLop));
                    bangDiem.data.AddValue(CommandName.select, new SqlParameter("@HK", hocKi));
                    bangDiem.data.AddValue(CommandName.select, new SqlParameter("@MaGiaoVien", MaGV));
                    s[0] = bangDiem.ReadTable("Diem");
                    if (s[0] == "")
                    {
                        table.Columns[0].ReadOnly = true;
                        table.Columns[1].ReadOnly = true;
                        table.Columns[10].ReadOnly = true;
                        for (int i = 2; i < 10; i++)
                            table.Columns[i].DefaultValue = 0;
                        foreach (DataRow dr in table.Rows)
                            for (int i = 2; i < 10; i++)
                                if (dr[i] == DBNull.Value)
                                    dr[i] = 0;
                        //SqlParameter
                        bangDiem.data.ClearValue(CommandName.update);
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@IDLop", IDLop));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@MaGV", MaGV));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@HocKi", hocKi));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@MaHS", SqlDbType.VarChar, 10, "MaHocSinh"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Mieng1", SqlDbType.Decimal, 4, "Điểm miệng lần 1"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Mieng2", SqlDbType.Decimal, 4, "Điểm miệng lần 2"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Diem151", SqlDbType.Decimal, 4, "Điểm 15 phút lần 1"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Diem152", SqlDbType.Decimal, 4, "Điểm 15 phút lần 2"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Diem451", SqlDbType.Decimal, 4, "Điểm 45 phút lần 1"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Diem452", SqlDbType.Decimal, 4, "Điểm 45 phút lần 2"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@CuoiKi", SqlDbType.Decimal, 4, "Điểm cuối kì"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@YThuc", SqlDbType.Decimal, 4, "Điểm ý thức"));
                        bangDiem.data.AddValue(CommandName.update, new SqlParameter("@Nhanxet", SqlDbType.NVarChar, 20, "Nhận Xét"));
                    }
                }
            }
            return s;
        }
        public string Update()
        {
            return bangDiem.Update();
        }
    }
}
