﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using qldDAL;
namespace qldBLL
{
    public class BangDiem
    {
        Table Lop, bangDiem, bangDiem_ChiTiet_BM, bangDiem_ChiTiet_HS;
        TimLop timLop;
        public DataTable DS_Lop
        {
            get
            {
                return Lop.table;
            }
        }
        public DataTable table
        {
            get
            { 
                return bangDiem.table;
            }
        }
        public DataTable table_ChiTiet_BM
        {
            get
            {
                return bangDiem_ChiTiet_BM.table;
            }
        }
        public DataTable table_ChiTiet_HS
        {
            get
            {
                return bangDiem_ChiTiet_HS.table;
            }
        }
        public BangDiem(Session session)
        {
            timLop = new TimLop(session, "concat(GiaoVien.HoDem, SPACE(1), GiaoVien.Ten) as 'ChuNhiem', Lop.LoaiHinhDaoTao");

            Lop = new Table(session);
            //Tạo select danh sách lớp giảng dậy
            Lop.select.Add(new QuerySelect());
            Lop.select[0].AddString(EnumQuery.select, "Lop.NamHoc, Lop.Lop, Lop.TenLop, Lop.IDLop");
            Lop.select[0].AddString(EnumQuery.from, "Lop");
            Lop.select[0].AddString(EnumQuery.order, "order by Lop.NamHoc, Lop.Lop, Lop.TenLop desc");
            Lop.SetCommand(Statement.select);

            bangDiem = new Table(session);
            bangDiem_ChiTiet_BM = new Table(session);
            bangDiem_ChiTiet_HS = new Table(session);
        }
        public string Init()
        {
            string s = Lop.ReadTable("Lop");
            return s;

        }
        public List<string> ReadTable(int namHoc, int Khoi, string TenLop, int HocKi)
        {
            List<string> s = new List<string>();
            string idLop = "";
            foreach (DataRow dr in DS_Lop.Rows)
                if (dr[0].ToString() == namHoc.ToString())
                    if (dr[1].ToString() == Khoi.ToString())
                        if (dr[2].ToString() == TenLop)
                            idLop = dr[3].ToString();
            if(idLop != "")
            {
                //Lấy Thông tin Lớp
                s.Add(timLop.Search(IDLop: idLop, LaythongTin_CN: true));
                s.Add(timLop.table.Rows[0][0].ToString());
                s.Add(timLop.table.Rows[0][1].ToString());
                //Lấy Bảng điểm
                bangDiem.data.ClearValue(CommandName.select);
                bangDiem.data.SetCommand(CommandName.select, "BangDiemHocKi",CommandType.StoredProcedure);
                bangDiem.data.AddValue(CommandName.select, new SqlParameter("@IDLop", idLop));
                bangDiem.data.AddValue(CommandName.select, new SqlParameter("@HocKi", HocKi));
                s.Add(bangDiem.ReadTable("BangDiem"));
            }
            return s;
        }
        public List<string> ReadTable(int namHoc, int Khoi, string TenLop, int index_HS, int HocKi)
        {
            List<string> s = new List<string>();
            string idLop = "";
            foreach (DataRow dr in DS_Lop.Rows)
                if (dr[0].ToString() == namHoc.ToString())
                    if (dr[1].ToString() == Khoi.ToString())
                        if (dr[2].ToString() == TenLop)
                            idLop = dr[3].ToString();
            if (idLop != "")
            {
                //Lấy Bảng điểm
                bangDiem_ChiTiet_HS.data.ClearValue(CommandName.select);
                bangDiem_ChiTiet_HS.data.SetCommand(CommandName.select, "BangDiem_HS", CommandType.StoredProcedure);
                bangDiem_ChiTiet_HS.data.AddValue(CommandName.select, new SqlParameter("@IDLop", idLop));
                bangDiem_ChiTiet_HS.data.AddValue(CommandName.select, new SqlParameter("@HK", HocKi));
                bangDiem_ChiTiet_HS.data.AddValue(CommandName.select, new SqlParameter("@MaHocSinh", table.Rows[index_HS][0]));
                s.Add(bangDiem_ChiTiet_HS.ReadTable("BangDiem_ChiTiet"));
            }
            return s;
        }
        public List<string> ReadTable(int namHoc, int Khoi, string TenLop, string MonHoc, int HocKi)
        {
            List<string> s = new List<string>();
            string idLop = "";
            foreach (DataRow dr in DS_Lop.Rows)
                if (dr[0].ToString() == namHoc.ToString())
                    if (dr[1].ToString() == Khoi.ToString())
                        if (dr[2].ToString() == TenLop)
                            idLop = dr[3].ToString();
            if (idLop != "")
            {
                //Lấy Bảng điểm
                bangDiem_ChiTiet_BM.data.ClearValue(CommandName.select);
                bangDiem_ChiTiet_BM.data.SetCommand(CommandName.select, "BangDiem_BM", CommandType.StoredProcedure);
                bangDiem_ChiTiet_BM.data.AddValue(CommandName.select, new SqlParameter("@IDLop", idLop));
                bangDiem_ChiTiet_BM.data.AddValue(CommandName.select, new SqlParameter("@HK", HocKi));
                bangDiem_ChiTiet_BM.data.AddValue(CommandName.select, new SqlParameter("@BoMon", MonHoc));
                s.Add(bangDiem_ChiTiet_BM.ReadTable("BangDiem_ChiTiet"));
            }
            return s;
        }
        public string Update()
        {
            return bangDiem.Update();
        }
    }
}
