﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qldBLL
{
    public interface Query
    {
        void Clear(EnumQuery query = EnumQuery.Null);
        void AddString(EnumQuery enumQuery, string var);
        string GetQuery();
    }
}
