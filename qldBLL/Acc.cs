﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using qldDAL;
using System.Data.SqlClient;
namespace qldBLL
{
    public class Acc
    {
        DataRow info;
        Adapter table;
        public bool isLogin;
        Session session;
        public Acc(Session session)
        {
            this.session = session;
            this.table = new Adapter(session.connect);
            table.SetCommand(CommandName.select, "AccInfomation", CommandType.StoredProcedure);
        }

        public int Login(string acc, string pass)
        {
            table.ClearValue(CommandName.select);
            table.AddValue(CommandName.select, new System.Data.SqlClient.SqlParameter( "@acc", acc));
            table.AddValue(CommandName.select, new System.Data.SqlClient.SqlParameter("@pass", pass));
            return GetInfo();
        }

        public void Logout()
        {
            info = null;
            isLogin = false;
        }

        public int GetInfo()
        {
            try
            {
                DataTable dt = table.GetTable("TaiKhoan");
                if (dt.Rows.Count == 0)
                    return 1;   // Không tìm thấy tài khoản yêu cầu
                info = dt.Rows[0];
                isLogin = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 2;   // Lỗi với cơ sở dữ liệu
            }
            return 0;
        }

        public string DoiThongTinDangNhap(string matKhau = null,
            string matKhauCap_2 = null,
            string cauHoi = null,
            string traLoi = null)
        {
            QueryUpdate update = new QueryUpdate();
            update.AddString(EnumQuery.from, "TaiKhoan");
            update.AddString(EnumQuery.where, "MaGiaoVien = '" + maGV + "'");
            //cập nhật mật khẩu
            if (matKhau != null)
            {
                update.AddString(EnumQuery.set, "MatKhau = '" + matKhau +"'");
            }
            //cập nhật mật khẩu cấp 2
            if (matKhauCap_2 != null)
            {
                update.AddString(EnumQuery.set, "MatKhauCap_2 = '" + matKhauCap_2 +"'");
            }
            //cập nhật câu hỏi
            if (cauHoi != null)
            {
                update.AddString(EnumQuery.set, "CauHoiBiMat = '" + cauHoi +"'");
            }
            //cập nhật trả lời
            if (traLoi != null)
            {
                update.AddString(EnumQuery.set, "CauTraLoi = '" + traLoi +"'");
            }
            Console.WriteLine(update.GetQuery());
            SqlCommand c = new SqlCommand(update.GetQuery(), session.connect.conn);
            try
            {
                c.ExecuteNonQuery();

                if (matKhau != null)
                    this.matKhau = matKhau;
                if (matKhauCap_2 != null)
                    this.matKhau_2 = matKhauCap_2;
                if (cauHoi != null)
                    this.cauHoi = cauHoi;
                if (traLoi != null)
                    this.traLoi = traLoi;
            }
            catch (Exception)
            {
                return "Lỗi kết nối với cơ sở dữ liệu";
            }
            return "";
        }

        public string DoiThongTinGiaoVien(
            string hoDem = "",
            string ten = "",
            string ngaySinh = "",
            int gioiTinh = -1,
            string diaChi = "",
            string email = "",
            string sdt = "")
        {
            //Ten
            if (ten == "")
                return "Tên không được bỏ trống";
            //NgaySinh
            try
            {
                if (ngaySinh == "")
                    ngaySinh = this.ngaySinh;
                Convert.ToDateTime(ngaySinh);
            }
            catch(Exception)
            {
                return "Ngày sinh khong hợp lệ";
            }

            SqlCommand c = new SqlCommand("update GiaoVien set " +
                "HoDem = N'" + hoDem + "'," +
                "Ten = N'" + ten + "'," +
                "NgaySinh = N'" + ngaySinh + "'," +
                "GioiTinh = " + gioiTinh + "," +
                "DiaChi = N'" + diaChi + "'," +
                "Email = N'" + email + "'," +
                "SDT = N'" + sdt + "' " + 
                "where MaGiaoVien = N'" + maGV + "'",session.connect.conn);
            try
            {
                //c.Connection.Open();
                c.ExecuteNonQuery();

                this.ten = ten;
                this.hoDem = hoDem;
                this.ngaySinh = ngaySinh;
                // Gioi tinh
                this.gioiTinh = (gioiTinh == -1 ? this.gioiTinh : gioiTinh);
                this.diaChi = diaChi;
                this.email = email;
                this.sdt = sdt;
            }
            catch(Exception)
            {
                return "Lỗi kết nối với cơ sở dữ liệu";
            }
            return "";
        }
        //.......................................................
        //Thông tin đăng nhập
        public string maGV
        {
            get
            {
                if (info["MaGiaoVien"] == DBNull.Value)
                    return "";
                return info["MaGiaoVien"].ToString();
            }
        }
        public string matKhau
        {
            set
            {
                info["MatKhau"] = value;
            }
            get
            {
                if (info["MatKhau"] == DBNull.Value)
                    return "";
                return info["MatKhau"].ToString();
            }
        }
        public string matKhau_2
        {
            set
            {
                info["MatKhauCap_2"] = value;
            }
            get
            {
                if (info["MatKhauCap_2"] == DBNull.Value)
                    return "";
                return info["MatKhauCap_2"].ToString();
            }
        }
        public string cauHoi
        {
            set
            {
                info["CauHoiBiMat"] = value;
            }
            get
            {
                if (info["CauHoiBiMat"] == DBNull.Value)
                    return "";
                return info["CauHoiBiMat"].ToString();
            }
        }
        public string traLoi
        {
            set
            {
                info["CauTraLoi"] = value;
            }
            get
            {
                if (info["CauTraLoi"] == DBNull.Value)
                    return "";
                return info["CauTraLoi"].ToString();
            }
        }
        public int loaiTaiKhoan
        {
            set
            {
                info["LoaiTaiKhoan"] = value;
            }
            get
            {
                if (info["LoaiTaiKhoan"] == DBNull.Value)
                    return -2;
                return Convert.ToInt32(info["LoaiTaiKhoan"]);
            }
        }
        //Thông tin Giáo Viên
        public string BoMon
        {
            set
            {
                info["BoMon"] = value;
            }
            get
            {
                if (info["BoMon"] == DBNull.Value)
                    return "";
                return info["BoMon"].ToString();
            }
        }
        public string hoDem
        {
            set
            {
                info["HoDem"] = value;
            }
            get
            {
                if (info["HoDem"] == DBNull.Value)
                    return "";
                return info["HoDem"].ToString();
            }
        }
        public string ten
        {
            set
            {
                info["Ten"] = value;
            }
            get
            {
                if (info["Ten"] == DBNull.Value)
                    return "";
                return info["Ten"].ToString();
            }
        }
        public string soCMT
        {
            set
            {
                info["SoCMT"] = value;
            }
            get
            {
                if ( info["SoCMT"] == DBNull.Value)
                    return null;
                return info["SoCMT"].ToString();
            }
        }

        public string ngaySinh//datetime
        {
            set
            {
                info["NgaySinh"] = value;
            }
            get
            {
                if (info["NgaySinh"] == DBNull.Value)
                    return "";
                DateTime d = Convert.ToDateTime(info["NgaySinh"]);
                string s = d.Day + "-" + d.Month + "-" + d.Year;
                return s;
            }
        }
        public int gioiTinh //boolean
        {
            set
            {
                info["GioiTinh"] = value;
            }
            get
            {
                if (info["GioiTinh"] == DBNull.Value)
                    return - 1;
                return Convert.ToInt16(info["GioiTinh"]);
            }
        }
        public string ngayVaoTruong//datetime
        {
            set
            {
                info["NgayVaoTruong"] = Convert.ToDateTime(value);
            }
            get
            {
                if (info["NgayVaoTruong"] == DBNull.Value)
                    return "";
                return info["NgayVaoTruong"].ToString();
            }
        }
        public string ngayRoiTruong//datetime
        {
            set
            {
                info["NgayRoiTruong"] = Convert.ToDateTime(value);
            }
            get
            {
                if (info["NgayRoiTruong"] == DBNull.Value)
                    return "";
                return info["NgayRoiTruong"].ToString();
            }
        }
        public string diaChi
        {
            set
            {
                info["DiaChi"] = value;
            }
            get
            {
                if (info["DiaChi"] == DBNull.Value)
                    return "";
                return info["DiaChi"].ToString();
            }
        }
        public string email
        {
            set
            {
                info["Email"] = value;
            }
            get
            {
                if (info["Email"] == DBNull.Value)
                    return "";
                return info["Email"].ToString();
            }
        }
        public string sdt
        {
            set
            {
                info["SDT"] = value;
            }
            get
            {
                if (info["SDT"] == DBNull.Value)
                    return "";
                return info["SDT"].ToString();
            }
        }

    }
}
