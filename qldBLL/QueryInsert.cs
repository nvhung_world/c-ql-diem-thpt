﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qldBLL
{
    public class QueryInsert : Query
    {
        string  from, column, value;
        public QueryInsert()
        {
            from =column = value = "";
        }
        public void Clear(EnumQuery query = EnumQuery.Null)
        {
            switch (query)
            {
                case EnumQuery.from:
                    from = "";
                    break;
                case EnumQuery.column:
                    column = "";
                    break;
                case EnumQuery.value:
                    value = "";
                    break;
                case EnumQuery.Null:
                    from = column = value = "";
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh insert");
            }
        }
        public void AddString(EnumQuery query, string var)
        {
            switch (query)
            {
                case EnumQuery.from:
                    from = var;
                    break;
                case EnumQuery.column:
                    if (column == "")
                        column = var;
                    else
                        column = ", " + var;
                    break;
                case EnumQuery.value:
                    if (value == "")
                        value = var;
                    else
                        value = ", " + var;
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh insert");
            }
            
        }
        public string GetQuery()
        {
            if (from == "")
                throw new Exception("table khong được bỏ trống");
            if (value == "")
                throw new Exception("danh sách value trong lệnh insert into " + from + " không được bỏ trống");
            string query = "insert into " + from + (column == "" ? "" : " (" + column + ")") + " values(" + value  + ")";
            return query;
        }
    }
}
