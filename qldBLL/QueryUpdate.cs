﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qldBLL
{
    public class QueryUpdate : Query
    {
        string from, where, set;
        public QueryUpdate()
        {
            from = where = set = "";
        }

        public void Clear(EnumQuery query = EnumQuery.Null)
        {
            switch(query)
            {
                case EnumQuery.from:
                    from = "";
                    break;
                case EnumQuery.where:
                    where = "";
                    break;
                case EnumQuery.set:
                    set = "";
                    break;
                case EnumQuery.Null:
                    from = where = set = "";
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh update");
            }
        }
        public void AddString(EnumQuery query, string var)
        {
            switch (query)
            {
                case EnumQuery.from:
                    from = var;
                    break;
                case EnumQuery.set:
                    if (set == "")
                        set = var;
                    else
                        set += ", " + var;
                    break;
                case EnumQuery.where:
                    if (where == "")
                        where = var;
                    else
                        where += " and " + var;
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh update");
            }
            
        }
        public string GetQuery()
        {
            if (from == "")
                throw new Exception("table khong được bỏ trống");
            if (set == "")
                throw new Exception("danh sách set trong lệnh update " + from + " không được bỏ trống");
            string query = "update " + from + " set " + set + (where == "" ? "" : " where " + where);
            return query;
        }
    }
}
