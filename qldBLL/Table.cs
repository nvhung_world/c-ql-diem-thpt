﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using qldDAL;

namespace qldBLL
{
    public class Table
    {
        public DataTable table
        {
            get
            {
                return data.table;
            }
        }
        public Adapter data;
        public List<Query> insert, delete, update, select;
        public Table(Session session)
        {
            data = new Adapter(session.connect);
            insert = new List<Query>();
            delete = new List<Query>();
            update = new List<Query>();
            select = new List<Query>();
        }

        public string ReadTable(string tableName)
        {
            string s = "";
            try
            {
                data.GetTable(tableName);
            }
            catch(Exception ex)
            {
                s = ex.Message;
            }
            return s;
        }
        public string Update()
        {
            string s = "";
            try
            {
                data.Update();
            }
            catch (Exception ex)
            {
                s = ex.Message;
            }
            return s;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetCommand(Statement name)
        {
            string s = "";
            switch (name)
            {
                case Statement.select:
                    if (select.Count == 0)
                        throw new Exception("Danh sách select trống");
                    foreach (Query q in select)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.select, s);
                    break;
                case Statement.delete:
                    if (delete.Count == 0)
                        throw new Exception("Danh sách delete trống");
                    foreach (Query q in delete)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.delete, s);
                    break;
                case Statement.insert:
                    if (insert.Count == 0)
                        throw new Exception("Danh sách insert trống");
                    foreach (Query q in insert)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.insert, s);
                    break;
                case Statement.update:
                    if (update.Count == 0)
                        throw new Exception("Danh sách update trống");
                    foreach (Query q in update)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.update, s);
                    break;
            }
        }
        public void SetCommand(Statement name, CommandType type)
        {
            string s = "";
            switch(name)
            {
                case Statement.select:
                    if (select.Count == 0)
                        throw new Exception("Danh sách select trống");
                    foreach (Query q in select)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.select, s, type);
                    break;
                case Statement.delete:
                    if (delete.Count == 0)
                        throw new Exception("Danh sách delete trống");
                    foreach (Query q in delete)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.delete, s, type);
                    break;
                case Statement.insert:
                    if (insert.Count == 0)
                        throw new Exception("Danh sách insert trống");
                    foreach (Query q in insert)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.insert, s, type);
                    break;
                case Statement.update:
                    if (update.Count == 0)
                        throw new Exception("Danh sách update trống");
                    foreach (Query q in update)
                        s += q.GetQuery() + " ";
                    data.SetCommand(CommandName.update, s, type);
                    break;
            }
        }
        public void AddValue(Statement name, SqlParameter value)
        {
            switch (name)
            {
                case Statement.select:
                    data.AddValue(CommandName.select, value);
                    break;
                case Statement.delete:
                    data.AddValue(CommandName.delete, value);
                    break;
                case Statement.insert:
                    data.AddValue(CommandName.insert, value);
                    break;
                case Statement.update:
                    data.AddValue(CommandName.update, value);
                    break;
            }
        }
        public void ClearValue(Statement name)
        {
            switch (name)
            {
                case Statement.select:
                    data.ClearValue(CommandName.select);
                    break;
                case Statement.delete:
                    data.ClearValue(CommandName.delete);
                    break;
                case Statement.insert:
                    data.ClearValue(CommandName.insert);
                    break;
                case Statement.update:
                    data.ClearValue(CommandName.update);
                    break;
            }
        }
    }
}
