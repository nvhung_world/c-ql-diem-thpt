﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using qldDAL;
namespace qldBLL
{
    public class Session
    {
        bool isOpen
        {
            get
            {
                return connect.isConnect;
            }
        }
        bool isLogin
        {
            get
            {
                return acc.isLogin;
            }
        }
        public Connect connect;
        public Acc acc;
        public Session()
        {
            connect = new Connect();
            acc = new Acc(this);
        }
        public int Login(string acc, string pass)
        {
            if (connect.isConnect)
            {
                int i = this.acc.Login(acc, pass);
                return i;
            }
            else
                return 2;
        }
        public void Logout()
        {
            acc.Logout();
        }
        public int Open()
        {
            return connect.Open();
        }
        public void Close()
        {
            connect.Close();
        }
    }
}
