﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using qldDAL;
using System.Data;
using System.Data.SqlClient;

namespace qldBLL
{
    public class PhanLopHS
    {
        Table data;
        public DataTable table
        {
            get
            {
                return data.table;
            }
        }
        string idLop;
        public PhanLopHS(Session session, string idLop)
        {
            this.idLop = idLop;
            data = new Table(session);
            SetupInsert();
            SetupDelete();
            //Tạo select command
            data.select.Add(new QuerySelect());
            data.select[0].AddString(EnumQuery.select, "HocSinh.MaHocSinh, concat(HocSinh.HoDem, SPACE(1), HocSinh.Ten) as 'HoTen', HocSinh.KhoaHoc, HocSinh.GioiTinh");
            data.select[0].AddString(EnumQuery.from, "HocSinh, PhanLop");
        }

        public string Search()
        {
            data.select[0].Clear(EnumQuery.where);
            data.select[0].AddString(EnumQuery.where, "HocSinh.MaHocSinh = PhanLop.MaHocSinh");
            data.select[0].AddString(EnumQuery.where, "PhanLop.IDLop = " + idLop);

            data.SetCommand(Statement.select);

            return data.ReadTable("GiaoVien");
        }

        public string Update()
        {
            return data.Update();
        }
        public void SetupInsert()
        {
            data.insert.Add(new QueryInsert());
            data.insert[0].AddString(EnumQuery.from, "PhanLop");
            data.insert[0].AddString(EnumQuery.column, "IDLop,MaHocSinh");
            data.insert[0].AddString(EnumQuery.value, "@IDLopInsert,@MaHocSinh");
            data.SetCommand(Statement.insert);

            data.AddValue(Statement.insert, new SqlParameter("@IDLopInsert", idLop));
            data.AddValue(Statement.insert, new SqlParameter("@MaHocSinh", SqlDbType.VarChar, 10, "MaHocSinh"));
        }

        public void SetupDelete()
        {
            data.delete.Add(new QueryDelete());
            data.delete[0].AddString(EnumQuery.from, "PhanLop");
            data.delete[0].AddString(EnumQuery.where, "IDLop = @IDLopDelete and MaHocSinh = @MaHS");
            data.SetCommand(Statement.delete);
            data.AddValue(Statement.delete, new SqlParameter("@IDLopDelete", idLop));
            data.AddValue(Statement.delete, new SqlParameter("@MaHS", SqlDbType.VarChar, 10, "MaHocSinh"));
        }

        public void SetupUpdate()
        {
        }
    }
}
