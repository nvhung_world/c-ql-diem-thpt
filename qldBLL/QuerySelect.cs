﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qldBLL
{
    public class QuerySelect : Query
    {
        string select, from, where, top, order;
        public QuerySelect()
        {
            select = from = where = top = order = "";
        }

        public void Clear(EnumQuery query = EnumQuery.Null)
        {
            switch (query)
            {
                case EnumQuery.select:
                    select = "";
                    break;
                case EnumQuery.from:
                    from = "";
                    break;
                case EnumQuery.where:
                    where = "";
                    break;
                case EnumQuery.top:
                    top = "";
                    break;
                case EnumQuery.order:
                    order = "";
                    break;
                case EnumQuery.Null:
                    select = from = where = top = order = "";
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh select");
            }
        }
        public void AddString(EnumQuery query, string var)
        {
            switch (query)
            {
                case EnumQuery.select:
                    if (select == "")
                        select = var;
                    else
                        select += ", " + var;
                    break;
                case EnumQuery.from:
                    if (from == "")
                        from = var;
                    else
                        from += ", " + var;
                    break;
                case EnumQuery.where:
                    if (where == "")
                        where = var;
                    else
                        where += " and " + var;
                    break;
                case EnumQuery.top:
                    top = var;
                    break;
                case EnumQuery.order:
                    order = var;
                    break;
                default:
                    throw new Exception("Tham số " + query + " Là không có trong lệnh select");
            }
            
        }
        public string GetQuery()
        {
            if (from == "")
                throw new Exception("table khong được bỏ trống");
            if (select == "")
                throw new Exception("danh sách select trong lệnh select " + from + " không được bỏ trống");
            string query = "select " + (top == "" ? "": "top " + top + " ") + select + " from " + from + (where == "" ? "": " where " + where ) + (order == "" ? "": " " + order);
            return query;
        }
    }
}
