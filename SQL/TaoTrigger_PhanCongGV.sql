﻿create trigger PhanCongGV_Tg
on PhanCongGV
for insert, update
as
	declare @IDLop bigint
	declare @BoMon nvarchar(20)
	declare @MaGV Varchar(10)
	declare @IDPhanCong bigint
	select @IDLop = inserted.IDLop,
		@BoMon = GiaoVien.BoMon,
		@MaGV = inserted.MaGiaoVien,
		@IDPhanCong = inserted.IDPhanCongGV
	from inserted, GiaoVien
	where GiaoVien.MaGiaoVien = inserted.MaGiaoVien
	--Kiểm tra xem Giáo viên này đã được phân công vào lớp hay chưa
	if @MaGV = any
		(select MaGiaoVien
		 from PhanCongGV
		 where PhanCongGV.IDLop = @IDLop
			and IDPhanCongGV != @IDPhanCong)
		begin
			rollback transaction
		end
	--Kiểm tra xem có giáo viên cùng bộ môn đã được phân công vào lớp hay chưa
	if @BoMon = any
		(select GiaoVien.BoMon
		 from PhanCongGV, GiaoVien
		 where PhanCongGV.IDLop = @IDLop
			and PhanCongGV.MaGiaoVien = GiaoVien.MaGiaoVien
			and IDPhanCongGV != @IDPhanCong)
		begin
			rollback transaction
		end