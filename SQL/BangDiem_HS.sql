create proc [dbo].[BangDiem_HS](@IDLop bigint, @HK int, @MaHocSinh varchar(10))
as
begin
	declare @IDPhanLop bigint

	select @IDPhanLop = PhanLop.IDPhanLop
	from PhanLop
	where PhanLop.MaHocSinh = @MaHocSinh
		and PhanLop.IDLop = @IDLop

	select GiaoVien.BoMon,
		Diem.DiemYThuc as 'Điểm ý thức',
		Diem.DiemMieng1 as 'Điểm miệng lần 1', Diem.DiemMieng2 as 'Điểm miệng lần 2',
		Diem.Diem15_1 as 'Điểm 15 phút lần 1', Diem.Diem15_2 as 'Điểm 15 phút lần 2',
		Diem.Diem45_1 as 'Điểm 45 phút lần 1', Diem.Diem45_2 as 'Điểm 45 phút lần 2',
		Diem.DiemCuoiKi as 'Điểm cuối kì', Diem.DiemTongKet as 'Điểm tổng kết',
		Diem.NhanXet  as 'Nhận Xét'
	from GiaoVien, (PhanCongGV left outer join Diem on PhanCongGV.IDPhanCongGV = Diem.IDPhanCongGV and Diem.IDPhanLop = @IDPhanLop and Diem.HocKi = @HK)
	where GiaoVien.MaGiaoVien = PhanCongGV.MaGiaoVien
		and PhanCongGV.IDLop = @IDLop
end