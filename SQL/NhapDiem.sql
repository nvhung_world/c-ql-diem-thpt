create proc NhapDiem(@MaGV varchar(10), @MaHS varchar(10), @IDLop bigint, @HocKi int,@Mieng1 decimal(4,2),@Mieng2 decimal(4,2),@Diem151 decimal(4,2),@Diem152 decimal(4,2),@Diem451 decimal(4,2),@Diem452 decimal(4,2),@CuoiKi decimal(4,2),@YThuc decimal(4,2),@NhanXet nvarChar(20))
as
begin
	declare @IDPhanLop bigint
	declare @IDPhanCong bigint

	select @IDPhanLop = IDPhanLop
	from PhanLop
	where MaHocSinh = @MaHS
		and IDLop = @IDLop
	select @IDPhanCong = IDPhanCongGV
	from PhanCongGV
	where MaGiaoVien = @MaGV
		and IDLop = @IDLop
	if @HocKi = any(select HocKi from Diem where IDPhanLop = @IDPhanLop and IDPhanCongGV = @IDPhanCong)
		begin
			update Diem 
			set DiemMieng1 = @Mieng1, DiemMieng2 = @Mieng2, Diem15_1 = @Diem151, Diem15_2 = @Diem152, Diem45_1 = @Diem451 , Diem45_2 = @Diem452, DiemCuoiKi = @CuoiKi, DiemYThuc = @YThuc, NhanXet = @NhanXet
			where IDPhanCongGV = @IDPhanCong and IDPhanLop = @IDPhanLop	
		end
	else 
		begin
			insert into Diem(IDPhanLop,IDPhanCongGV, HocKi, DiemMieng1,DiemMieng2, Diem15_1,Diem15_2,Diem45_1,Diem45_2,DiemCuoiKi,DiemYThuc,NhanXet)
			values(@IDPhanLop,@IDPhanCong, @HocKi,@Mieng1,@Mieng2,@Diem151,@Diem152,@Diem451,@Diem452,@CuoiKi,@YThuc,@NhanXet)
		end
end