create trigger [dbo].[KQHT_Trigger]
on [dbo].[KetQuaHocTap]
for insert, update
as
	declare @IDKQHT bigint
	declare @IDPhanLop bigint
	declare @HocKi int
	select @IDKQHT = inserted.IDKQHT, @IDPhanLop = IDPhanLop, @HocKi = HocKi
	from inserted
	
	update KetQuaHocTap
	set DiemYThuc = dbo.TinhDiemYThucHocKi(@IDPhanLop, @HocKi) , DiemTongKet = dbo.TinhDiemTongKetHocKi(@IDPhanLop,@HocKi), SuaLanCuoi = GETDATE()
	where IDKQHT = @IDKQHT