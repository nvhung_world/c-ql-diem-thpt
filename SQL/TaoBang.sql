﻿--drop table Diem
--drop table KetQuaHocTap
--Drop table PhanLop
--Drop table PhanCongGV
--drop table Lop
--drop table HocSinh
--drop table GiaoVien
--drop table TaiKhoan
--drop table BoMon

--Tao Bang TaiKhoan
create table TaiKhoan(
	MaGiaoVien varchar(10) not null,
	MatKhau nvarchar(30) not null,
	MatKhauCap_2 nvarchar(30),
	CauHoiBiMat nvarchar(50),
	CauTraLoi nvarchar(15),
	LoaiTaiKhoan int not null DEFAULT 1
		constraint chk_LoaiTaiKhoan 
			check(LoaiTaiKhoan > = -1 and LoaiTaiKhoan <=2),
			-- -1: Tu Choi
			-- 0: Admin
			-- 1: Giao Vien
	constraint pk_MaGiaoVien_TK
		primary key (MaGiaoVien)
	)
insert into TaiKhoan(MaGiaoVien,MatKhau,MatKhauCap_2,LoaiTaiKhoan) 
	values('admin','admin','admin',0)
--Tao Bang BoMon
create table BoMon(
	BoMon nvarchar(20) not null,
	HeSo int not null default 1,
	ChuNhiem varchar(10) not null,
	PhoChuNhiem varchar(10),
	NgayThanhLap date,
	constraint pk_TenMon
		primary key (BoMon)
	)
--Tao Bang GiaoVien
create table GiaoVien(
	MaGiaoVien varchar(10) not null,
	BoMon nvarchar(20) ,
	HoDem nvarchar(30),
	Ten nvarchar(10) not null,
	SoCMT varchar(15),
	NgaySinh date,
	GioiTinh bit default 1,
	NgayVaoTruong date,
	NgayRoiTruong date,
	DiaChi nvarchar(60),
	Email nvarchar(40),
	SDT varchar(11),
	constraint pk_MaGiaoVien_GV
		primary key (MaGiaoVien),
	constraint fk_MaGiaoVien_GV
		foreign key(MaGiaoVien)
		references TaiKhoan(MaGiaoVien),
	constraint fk_BoMon
		foreign key(BoMon)
		references BoMon(BoMon),
	)
-- Tao Bang HocSinh
create table HocSinh(
	MaHocSinh varchar(10) not null,
	KhoaHoc int not null,
	HoDem nvarchar(30),
	Ten nvarchar(10) not null,
	NgaySinh date,
	GioiTinh bit default 1,
	DiaChi nvarchar(60),
	Email nvarchar(40),
	SDT varchar(11),
	PhuHuynh nvarchar(40),
	SDTPhuHuynh varchar(11),
	NhanXet nvarchar(10),
	NgayRaTruong date,
	constraint pk_MaHocSinh 
		primary key(MaHocSinh)
	)
--Tao Bang Lop
create table Lop(
	IDLop bigint not null IDENTITY(1,1),
	Lop int not null
		constraint chk_Lop check(Lop >= 10 and Lop <=12), 
	TenLop varchar(10) not null,
	LoaiHinhDaoTao nvarchar(20) not null default 'Lớp Thường',
	NamHoc int not null,
	GiaoVienChuNhiem varchar(10) not null,
	LopTruong varchar(10),
	constraint pk_IDLop
		primary key(IDLop),
	constraint fk_GiaoVienChuNhiem
		foreign key(GiaoVienChuNhiem)
		references GiaoVien(MaGiaoVien),
	constraint fk_LopTruong 
		foreign key(LopTruong)
		references HocSinh(MaHocSinh),
	)
-- Tao Bang PhanLop
create table PhanLop(
	IDPhanLop bigint not null IDENTITY(1,1),
	IDLop bigint not null,
	MaHocSinh varchar(10) not null,
	constraint pk_IDPhanLop
		primary key(IDPhanLop),
	constraint fk_IDLop_PL
		foreign key(IDLop)
		references Lop(IDLop),
	constraint fk_MaHocSinh_PL
		foreign key(MaHocSinh)
		references HocSinh(MaHocSinh),
	)
-- Tao Bang PhanCongGV
create table PhanCongGV(
	IDPhanCongGV bigint not null IDENTITY(1,1),
	IDLop bigint not null,
	MaGiaoVien varchar(10) not null,
	constraint pk_IDPhanCongGV
		primary key(IDPhanCongGV),
	constraint fk_IDLop_PC
		foreign key(IDLop)
		references Lop(IDLop),
	constraint fk_MaGiaoVien_PC
		foreign key(MaGiaoVien)
		references GiaoVien(MaGiaoVien),
	)
-- Tao Bang Diem
create table Diem(
	IDDiem bigint not null IDENTITY(1,1),
	IDPhanLop bigint not null,
	IDPhanCongGV bigint not null,
	HocKi int not null default 1,
	DiemYThuc decimal(4,2)  default 0
		constraint chk_DiemYThuc_Diem check(DiemYThuc >= 0 and DiemYThuc <=10), 
	DiemMieng1 decimal(4,2)  default 0
		constraint chk_DiemMieng1_Diem check(DiemMieng1 >= 0 and DiemMieng1 <=10), 
	DiemMieng2 decimal(4,2)  default 0
		constraint chk_DiemMieng2_Diem check(DiemMieng2 >= 0 and DiemMieng2 <=10), 
	Diem15_1 decimal(4,2)  default 0
		constraint chk_Diem15_1_Diem check(Diem15_1 >= 0 and Diem15_1 <=10), 
	Diem15_2 decimal(4,2)  default 0
		constraint chk_Diem15_2_Diem check(Diem15_2 >= 0 and Diem15_2 <=10), 
	Diem45_1 decimal(4,2)  default 0
		constraint chk_Diem45_1_Diem check(Diem45_1 >= 0 and Diem45_1 <=10), 
	Diem45_2 decimal(4,2)  default 0
		constraint chk_Diem45_2_Diem check(Diem45_2 >= 0 and Diem45_2 <=10), 
	DiemCuoiKi decimal(4,2)  default 0
		constraint chk_DiemCuoiKi check(DiemCuoiKi >= 0 and DiemCuoiKi <=10),
	DiemTongKet decimal(4,2)  default 0
		constraint chk_DiemTongKet_Diem check(DiemTongKet >= 0 and DiemTongKet <=10),
	NhanXet nvarchar(40),
	SuaLanCuoi date,
	constraint pk_IDDiem 
		primary key(IDDiem),
	constraint fk_IDPhanLop_Diem 
		foreign key(IDPhanLop)
		references PhanLop(IDPhanLop)
			on delete cascade
			on update cascade,
	constraint fk_IDPhanCongGV_Diem
		foreign key(IDPhanCongGV)
		references PhanCongGV(IDPhanCongGV)
			on delete cascade
			on update cascade
	)
-- Tao Bang KetQuaHocTap
create table KetQuaHocTap(
	IDKQHT bigint not null IDENTITY(1,1),
	IDPhanLop bigint not null,
	HocKi int not null default 1,
	DiemYThuc decimal(4,2)
		constraint chk_DiemYThuc_KQHT
			check(DiemYThuc >= 0 and DiemYThuc <=10), 
	DiemTongKet decimal(4,2)
		constraint chk_DiemTongKet_KQHT 
			check(DiemTongKet >= 0 and DiemTongKet <=10),
	NhanXet nvarchar(40),
	SuaLanCuoi date,
	constraint pk_IDKQHT
		primary key(IDKQHT),
	constraint fk_IDPhanLop_KQHT 
		foreign key(IDPhanLop)
		references PhanLop(IDPhanLop)
			on delete cascade
			on update cascade
	)
