﻿-- Tạo Trigger
create TRIGGER PhanLop_tg
on Phanlop
for insert, update
as
	begin
		declare @IDPhanLop bigint
		declare @IDLop bigint
		declare @MaHocSinh nvarchar(10)
		declare @NamHoc int

		select @IDPhanLop = inserted.IDPhanLop
			,@IDLop = inserted.IDLop
			,@MaHocSinh = inserted.MaHocSinh
			,@NamHoc = Lop.NamHoc
		from inserted, Lop
		where inserted.IDLop = Lop.IDLop
		--Kiểm tra Hoc sinh có thuộc lớp này không
		if @IDPhanLop != any
			(select IDPhanLop
			 from PhanLop
			 where MaHocSinh = @MaHocSinh
				and IDLop = @IDLop)
			begin
				rollback transaction
			end
		--kiểm tra học sinh có thuộc lớp khác cùng năm học
		if @IDLop = any
			(select Lop.IDLop
			 from Lop, PhanLop
			 where Lop.IDLop = PhanLop.IDLop
				and PhanLop.MaHocSinh = @MaHocSinh
				and lop.NamHoc = @NamHoc
				and lop.IDLop != @IDLop)
			begin
				rollback transaction
			end
	end