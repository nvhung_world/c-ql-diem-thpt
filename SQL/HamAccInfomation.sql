Create proc AccInfomation(@acc varchar(10), @pass nvarchar(30))
as 
	begin
		select TaiKhoan.MaGiaoVien,
			TaiKhoan.MatKhau,
			TaiKhoan.MatKhauCap_2,
			TaiKhoan.CauHoiBiMat,
			TaiKhoan.CauTraLoi,
			TaiKhoan.LoaiTaiKhoan,
			GiaoVien.BoMon,
			GiaoVien.HoDem,
			GiaoVien.Ten,
			GiaoVien.SoCMT,
			GiaoVien.NgaySinh,
			GiaoVien.GioiTinh,
			GiaoVien.NgayVaoTruong,
			GiaoVien.NgayRoiTruong,
			GiaoVien.DiaChi,
			GiaoVien.Email,
			GiaoVien.SDT
		from TaiKhoan full outer join GiaoVien on TaiKhoan.MaGiaoVien = GiaoVien.MaGiaoVien
		where TaiKhoan.MaGiaoVien = @acc
			and TaiKhoan.MatKhau = @pass
	end