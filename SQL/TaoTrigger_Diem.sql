﻿create trigger DiemTrigger
on Diem
for insert, update
as
	begin
		declare @IDDiem bigint
		declare @DiemTongKet decimal(4,2)
		declare @IDPhanLop bigint
		declare @HocKi bigint
		-- khởi tạo giá trị cho biến
		select @IDDiem = IDDIem, 
			@DiemTongKet = ((Diem15_1+Diem15_2+DiemMieng1+DiemMieng2) + (Diem45_1 + Diem45_2)*2 + DiemCuoiKi* 3) /11,
			@IDPhanLop = inserted.IDPhanLop,
			@HocKi = inserted.HocKi
		from inserted
		
		--Tính điểm tổng kết và Thời gian sửa lần cuối
		update Diem
		set DiemTongKet = @DiemTongKet, SuaLanCuoi = GETDATE()
		where IDDiem = @IDDiem
		--Phát sinh sự kiện tính điểm tổng kết cả kì
		if @IDPhanLop = any
			(select IDPhanLop
			 from KetQuaHocTap
			 where HocKi = @HocKi)
			begin
				update KetQuaHocTap
				set DiemYThuc = 0 ,DiemTongKet = 0
				where HocKi = @HocKi and IDPhanLop = @IDPhanLop
			end
		else
			begin
				insert into KetQuaHocTap(IDPhanLop,HocKi,DiemYThuc,DiemTongKet)
				values(@IDPhanLop,@HocKi, 0, 0)
			end
	end