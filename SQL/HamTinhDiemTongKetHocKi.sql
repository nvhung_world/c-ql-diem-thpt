create function TinhDiemTongKetHocKi(@IDPhanLop bigint, @HocKi int)
returns decimal(4,2)
as
begin
	declare @DiemTongKet decimal(4,2)

	select @DiemTongKet = SUM( DiemTongKet * BoMon.HeSo) / sum(BoMon.HeSo) 
	from Diem, PhanCongGV, BoMon, GiaoVien
	where Diem.IDPhanCongGV = PhanCongGV.IDPhanCongGV
		and PhanCongGV.MaGiaoVien = GiaoVien.MaGiaoVien
		and GiaoVien.BoMon = BoMon.BoMon
		and Diem.IDPhanLop = @IDPhanLop
		and Diem.HocKi = @HocKi
	group by Diem.IDPhanLop
	return @DiemTongKet
end