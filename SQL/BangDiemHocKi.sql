create proc [dbo].[BangDiemHocKi](@IDLop bigint, @HocKi int)
as
	begin
	----Kiểm tra nếu tồn tại bảng #TestTmp thì xóa đi và tạo lại bảng mới
	IF EXISTS (select * from dbo.sysobjects where id = object_id(N'TestTmp'))
	drop table TestTmp
	select * into TestTmp
	from
	(
		select Hs.MaHocSinh , CONCAT(Hs.HoDem, SPACE(1), Hs.Ten) as 'HoTen', GV.BoMon,D.DiemTongKet, Kq.DiemTongKet as 'TongKet'
		from PhanCongGV Pc
		join Diem D
		on Pc.IDPhanCongGV=D.IDPhanCongGV
		join PhanLop Pl
		on PL.IDPhanLop=D.IDPhanLop
		join GiaoVien Gv
		on Gv.MaGiaoVien = Pc.MaGiaoVien
		join HocSinh Hs
		on Hs.MaHocSinh = Pl.MaHocSinh
		join KetQuaHocTap Kq
		on Pl.IDPhanLop = Kq.IDPhanLop
		where D.HocKi = @HocKi
			and Kq.HocKi = @HocKi
			and Pc.IDLop = @IDLop
		) as a
	----- Thưc hiện truy vấn nâng cao
	DECLARE @STR_UNION varchar(20)set @STR_UNION = 'union all'
	DECLARE @SQLSUM nvarchar(max)
	DECLARE @SQL nvarchar(max) set @SQL = ''
	DECLARE @STRSBJ nvarchar(max) set @STRSBJ = ''
	DECLARE @STRSBJ_TMP nvarchar(max) set @STRSBJ_TMP = ''
	DECLARE @subjectName nvarchar(50)
	DECLARE ListSubject CURSOR FOR 
		select distinct GiaoVien.BoMon 
		from GiaoVien, PhanCongGV 
		where PhanCongGV.IDLop = @IDLop 
			and PhanCongGV.MaGiaoVien = GiaoVien.MaGiaoVien
	OPEN ListSubject
	FETCH NEXT FROM ListSubject INTO @subjectName
	WHILE @@FETCH_STATUS = 0
	BEGIN
	BEGIN TRY
	--
	set @SQLSUM = ''
	DECLARE @subjectNameChild nvarchar(50)
	DECLARE ListSubjectChild CURSOR FOR 
		select distinct GiaoVien.BoMon 
		from GiaoVien, PhanCongGV 
		where PhanCongGV.IDLop = @IDLop 
			and PhanCongGV.MaGiaoVien = GiaoVien.MaGiaoVien
	OPEN ListSubjectChild
	FETCH NEXT FROM ListSubjectChild INTO @subjectNameChild
	WHILE @@FETCH_STATUS = 0
	BEGIN
	BEGIN TRY
	if(@subjectNameChild=@subjectName)
	set @STRSBJ_TMP = @STRSBJ_TMP + 'DiemTongKet as '''+@subjectNameChild+''','
	else
	set @STRSBJ_TMP = @STRSBJ_TMP + '0 as '''+@subjectNameChild+''','
	set @SQLSUM = @SQLSUM + 'sum(['+@subjectNameChild+']) as '''+@subjectNameChild+''','
	END TRY
	BEGIN CATCH
	END CATCH
	FETCH NEXT FROM ListSubjectChild INTO @subjectNameChild
	END
	CLOSE ListSubjectChild
	DEALLOCATE ListSubjectChild
	set @STRSBJ = @STRSBJ_TMP
	set @STRSBJ_TMP=''
	--
	set @SQL = @SQL + 'select MaHocSinh,HoTen, '+@STRSBJ+' TongKet from
	TestTmp
	where BoMon=N'''+@subjectName+''''
	set @SQL = @SQL + CHAR(13) + CHAR(10)
	set @SQL = @SQL + @STR_UNION
	set @SQL = @SQL + CHAR(13) + CHAR(10)
	END TRY
	BEGIN CATCH
	END CATCH
	FETCH NEXT FROM ListSubject INTO @subjectName
	END
	CLOSE ListSubject
	DEALLOCATE ListSubject
	--print @SQLSUM
	set @SQL = substring(@SQL, 0, len(@SQL)-(len(@STR_UNION)+1))
	set @SQL = 'select MaHocSinh, HoTen, '+@SQLSUM+' TongKet
	from(' + CHAR(13) + CHAR(10)+ @SQL + CHAR(13) + CHAR(10) + ') as a' + CHAR(13) + CHAR(10)
	+'group by HoTen, MaHocSinh, TongKet'
	exec(@SQL)
	IF EXISTS (select * from dbo.sysobjects where id = object_id(N'TestTmp'))
	drop table TestTmp
end