create function TinhDiemYTHucHocKi(@IDPhanLop bigint, @HocKi int)
returns decimal(4,2)
as
begin
	declare @DiemYThuc decimal(4,2)

	select @DiemYThuc = SUM(Diem.DiemYThuc)/ COUNT(Diem.DiemYThuc)
	from Diem
	where Diem.IDPhanLop = @IDPhanLop
		and Diem.HocKi = @HocKi
	group by Diem.IDPhanLop
	return @DiemYThuc
end