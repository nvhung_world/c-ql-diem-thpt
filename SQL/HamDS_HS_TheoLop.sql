﻿create proc DS_HS_TheoLop(@IDLop int)
as
	begin
		select HocSinh.MaHocSinh, 
			HocSinh.KhoaHoc, 
			HocSinh.HoDem + ' ' + HocSinh.Ten as 'Họ tên',
			HocSinh.GioiTinh,
			HocSinh.DiaChi
		from PhanLop, HocSinh
		where PhanLop.IDLop = @IDLop
			and PhanLop.MaHocSinh = HocSinh.MaHocSinh
	end