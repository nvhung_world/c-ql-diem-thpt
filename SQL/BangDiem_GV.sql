create proc [dbo].[BangDiem_GV](@IDLop bigint, @HK int, @MaGiaoVien varchar(10))
as
begin
	declare @IDPhanCongGV bigint

	select @IDPhanCongGV = IDPhanCongGV
	from PhanCongGV
	where MaGiaoVien = @MaGiaoVien
		and IDLop = @IDLop

	select HocSinh.MaHocSinh, CONCAT(HocSinh.HoDem, SPACE(1), HocSinh.Ten) as 'Họ Tên',
		Diem.DiemYThuc as 'Điểm ý thức',
		Diem.DiemMieng1 as 'Điểm miệng lần 1', Diem.DiemMieng2 as 'Điểm miệng lần 2',
		Diem.Diem15_1 as 'Điểm 15 phút lần 1', Diem.Diem15_2 as 'Điểm 15 phút lần 2',
		Diem.Diem45_1 as 'Điểm 45 phút lần 1', Diem.Diem45_2 as 'Điểm 45 phút lần 2',
		Diem.DiemCuoiKi as 'Điểm cuối kì', Diem.DiemTongKet as 'Điểm tổng kết',
		Diem.NhanXet  as 'Nhận Xét'
	from HocSinh, (PhanLop left outer join Diem on PhanLop.IDPhanLop = Diem.IDPhanLop and Diem.IDPhanCongGV = @IDPhanCongGV and Diem.HocKi = @HK)
	where HocSinh.MaHocSinh = PhanLop.MaHocSinh
		and PhanLop.IDLop = @IDLop
end