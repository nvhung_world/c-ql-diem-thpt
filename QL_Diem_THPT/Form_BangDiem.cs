﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
using Microsoft.Reporting.WinForms;
namespace QL_Diem
{
    public partial class Form_BangDiem : Form
    {
        string tenLop;
        int Khoi, namHoc, hocKi;
        BangDiem Data;
        public Form_BangDiem(Session session)
        {
            InitializeComponent();
            Data = new BangDiem(session);
        }
        private void Form_Load(object sender, EventArgs e)
        {
            if( Data.Init()=="")
            {
                int namhoc = -1, khoi = 0;
                string tenLop = "";
                TreeNode namhocNode = new TreeNode(), khoiNode = new TreeNode();
                foreach(DataRow dr in Data.DS_Lop.Rows)
                {
                    if(namhoc != Convert.ToInt32( dr[0]))
                    {
                        namhoc = Convert.ToInt32(dr[0]);
                        treeView1.Nodes.Add(namhoc.ToString() + " - "+ (namhoc +1).ToString());
                        namhocNode = treeView1.Nodes[treeView1.Nodes.Count - 1];
                    }
                    if(khoi != Convert.ToInt32( dr[1]))
                    {
                        khoi = Convert.ToInt32(dr[1]);
                        namhocNode.Nodes.Add("Khối " + khoi.ToString());
                        khoiNode = namhocNode.Nodes[namhocNode.Nodes.Count - 1];
                    }
                    if(tenLop != dr[2].ToString())
                    {
                        tenLop = dr[2].ToString();
                        khoiNode.Nodes.Add(tenLop);
                    }
                }
            }
        }
        private void tree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Parent != null && e.Node.Parent.Parent != null)
            {
                try
                {
                    tenLop = e.Node.Text;
                    string khoiText = e.Node.Parent.Text;
                    Khoi = int.Parse(khoiText.Substring(5));
                    string namHocText = e.Node.Parent.Parent.Text;
                    namHoc = int.Parse(namHocText.Substring(0, namHocText.IndexOf(' ')));
                    hocKi = 1;
                    checkBox1.Checked = true;
                    checkBox2.Checked = false;
                    ReadData(namHoc, Khoi, tenLop, hocKi);
                    ComboBox_LoadData();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        private void HocKi_CLick(object sender, EventArgs e)
        {
            string s = (sender as CheckBox).Text;
            if(s == "Học Kì 1")
            {
                checkBox1.Checked = true;
                checkBox2.Checked = false;
                ReadData(namHoc, Khoi, tenLop, 1);
                ComboBox_LoadData();
            }
            if (s == "Học Kì 2")
            {
                checkBox1.Checked = false;
                checkBox2.Checked = true;
                ReadData(namHoc, Khoi, tenLop, 2);
                ComboBox_LoadData();
            }
        }
        private void ReadData(int namHoc, int Khoi, string tenLop, int hocKi)
        {
            this.tenLop = tenLop;
            this.Khoi = Khoi;
            this.namHoc = namHoc;
            this.hocKi = hocKi;
            List<string> s = Data.ReadTable(namHoc, Khoi, tenLop, hocKi);
            if (s[0] == "")
            {
                dataGridView1.DataSource = Data.table;
                label1.Text = "Tên lớp: " + tenLop;
                label2.Text = "Năm học: " + namHoc;
                label3.Text = "Giáo viên chủ nhiệm: " + s[1];
                label4.Text = "Loại hình đào tạo: " + s[2];
            }
            else
                Console.WriteLine(s[0]);
        }
        private void ComboBox_LoadData()
        {
            try
            {
                //lạp dữ liệu cho commonbox
                comboBox1.Items.Clear();
                comboBox1.Items.Add("Mọi học sinh");
                comboBox1.SelectedIndex = 0;
                foreach (DataRow dr in Data.table.Rows)
                    comboBox1.Items.Add(dr[1]);

                comboBox2.Items.Clear();
                comboBox2.Items.Add("Mọi môn học");
                comboBox2.SelectedIndex = 0;
                for (int i = 2; i < 10; i++)
                    comboBox2.Items.Add(Data.table.Columns[i].ColumnName);
            }
            catch(Exception)
            { }
        }
        private void HocSinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                if(comboBox2.SelectedIndex == 0)
                    ReadData(namHoc, Khoi, tenLop, hocKi);
            }
            else
            {
                comboBox2.SelectedIndex = 0;
                List<string> s = Data.ReadTable(namHoc, Khoi, tenLop, comboBox1.SelectedIndex - 1, hocKi);
                if (s[0] == "")
                {
                    dataGridView1.DataSource = Data.table_ChiTiet_HS;

                }
                else
                    Console.WriteLine(s[0]);
            }
        }
        private void MonHoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex == 0)
            {
                if (comboBox1.SelectedIndex == 0)
                    ReadData(namHoc, Khoi, tenLop, hocKi);
            }
            else
            {
                comboBox1.SelectedIndex = 0;
                List<string> s = Data.ReadTable(namHoc, Khoi, tenLop, comboBox2.Text, hocKi);
                if (s[0] == "")
                {
                    dataGridView1.DataSource = Data.table_ChiTiet_BM;
                }
                else
                    Console.WriteLine(s[0]);
            }
        }

    }
}
