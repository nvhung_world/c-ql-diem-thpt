﻿namespace QL_Diem
{
    partial class Form_Lop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TK_button = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LopBox = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.LT_Box = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.GVCN_Box = new System.Windows.Forms.TextBox();
            this.LHDT_Box = new System.Windows.Forms.ComboBox();
            this.NamHocBox = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.IDLopBox = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TenLopBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SoKQ = new System.Windows.Forms.NumericUpDown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CN_button = new System.Windows.Forms.Button();
            this.qL_Log1 = new QL_Diem.QL_Log();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SoKQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(175, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Số Kết Quả";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(342, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID Lớp";
            // 
            // TK_button
            // 
            this.TK_button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TK_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.TK_button.FlatAppearance.BorderSize = 0;
            this.TK_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TK_button.Location = new System.Drawing.Point(788, 3);
            this.TK_button.Name = "TK_button";
            this.TK_button.Size = new System.Drawing.Size(107, 44);
            this.TK_button.TabIndex = 1;
            this.TK_button.Text = "Tìm Kiếm";
            this.TK_button.UseVisualStyleBackColor = false;
            this.TK_button.Click += new System.EventHandler(this.TimKiem_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.LopBox);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LT_Box);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.GVCN_Box);
            this.panel2.Controls.Add(this.LHDT_Box);
            this.panel2.Controls.Add(this.NamHocBox);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(12, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(769, 56);
            this.panel2.TabIndex = 0;
            // 
            // LopBox
            // 
            this.LopBox.Location = new System.Drawing.Point(403, 4);
            this.LopBox.Mask = "00";
            this.LopBox.Name = "LopBox";
            this.LopBox.Size = new System.Drawing.Size(116, 20);
            this.LopBox.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(339, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Lớp trưởng";
            // 
            // LT_Box
            // 
            this.LT_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LT_Box.Location = new System.Drawing.Point(403, 30);
            this.LT_Box.MaxLength = 15;
            this.LT_Box.Name = "LT_Box";
            this.LT_Box.Size = new System.Drawing.Size(116, 20);
            this.LT_Box.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(81, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Giáo viên chủ nhiệm";
            // 
            // GVCN_Box
            // 
            this.GVCN_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GVCN_Box.Location = new System.Drawing.Point(191, 30);
            this.GVCN_Box.MaxLength = 15;
            this.GVCN_Box.Name = "GVCN_Box";
            this.GVCN_Box.Size = new System.Drawing.Size(116, 20);
            this.GVCN_Box.TabIndex = 15;
            // 
            // LHDT_Box
            // 
            this.LHDT_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LHDT_Box.FormattingEnabled = true;
            this.LHDT_Box.Items.AddRange(new object[] {
            "Lớp Thường",
            "Lớp Chuyên"});
            this.LHDT_Box.Location = new System.Drawing.Point(191, 3);
            this.LHDT_Box.MaxLength = 20;
            this.LHDT_Box.Name = "LHDT_Box";
            this.LHDT_Box.Size = new System.Drawing.Size(121, 21);
            this.LHDT_Box.TabIndex = 14;
            // 
            // NamHocBox
            // 
            this.NamHocBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.NamHocBox.Location = new System.Drawing.Point(592, 4);
            this.NamHocBox.Mask = "0000000";
            this.NamHocBox.Name = "NamHocBox";
            this.NamHocBox.Size = new System.Drawing.Size(72, 20);
            this.NamHocBox.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(534, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Năm Học";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(353, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Lớp";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Loại hình đào tạo";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.IDLopBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.TenLopBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.SoKQ);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TK_button);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(910, 50);
            this.panel1.TabIndex = 11;
            // 
            // IDLopBox
            // 
            this.IDLopBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.IDLopBox.Location = new System.Drawing.Point(387, 3);
            this.IDLopBox.Mask = "0000000000";
            this.IDLopBox.Name = "IDLopBox";
            this.IDLopBox.Size = new System.Drawing.Size(94, 20);
            this.IDLopBox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(487, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Tên Lớp";
            // 
            // TenLopBox
            // 
            this.TenLopBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TenLopBox.Location = new System.Drawing.Point(540, 3);
            this.TenLopBox.MaxLength = 10;
            this.TenLopBox.Name = "TenLopBox";
            this.TenLopBox.Size = new System.Drawing.Size(74, 20);
            this.TenLopBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(338, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Hiển thị tìm kiếm nâng cao";
            this.label3.Click += new System.EventHandler(this.AnHien_TimKiem_Click);
            // 
            // SoKQ
            // 
            this.SoKQ.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.SoKQ.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.SoKQ.Location = new System.Drawing.Point(243, 3);
            this.SoKQ.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.SoKQ.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.SoKQ.Name = "SoKQ";
            this.SoKQ.Size = new System.Drawing.Size(78, 20);
            this.SoKQ.TabIndex = 6;
            this.SoKQ.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 68);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(910, 265);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.data_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.data_DataError);
            this.dataGridView1.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.data_UserDeletedRow);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.data_UserDeletingRow);
            // 
            // CN_button
            // 
            this.CN_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CN_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.CN_button.Enabled = false;
            this.CN_button.FlatAppearance.BorderSize = 0;
            this.CN_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CN_button.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CN_button.Location = new System.Drawing.Point(778, 339);
            this.CN_button.Name = "CN_button";
            this.CN_button.Size = new System.Drawing.Size(144, 60);
            this.CN_button.TabIndex = 8;
            this.CN_button.Text = "Cập Nhật";
            this.CN_button.UseVisualStyleBackColor = false;
            this.CN_button.Click += new System.EventHandler(this.CapNhat_Click);
            // 
            // qL_Log1
            // 
            this.qL_Log1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qL_Log1.fromName = "Quản Lý Lớp";
            this.qL_Log1.Location = new System.Drawing.Point(12, 339);
            this.qL_Log1.Log = "";
            this.qL_Log1.Name = "qL_Log1";
            this.qL_Log1.Size = new System.Drawing.Size(760, 60);
            this.qL_Log1.TabIndex = 10;
            // 
            // Form_Lop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(934, 411);
            this.Controls.Add(this.qL_Log1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.CN_button);
            this.MinimumSize = new System.Drawing.Size(950, 450);
            this.Name = "Form_Lop";
            this.Text = "Quản Lý Danh Sách Lớp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Shown += new System.EventHandler(this.From_Shown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SoKQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QL_Log qL_Log1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button TK_button;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TenLopBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown SoKQ;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button CN_button;
        private System.Windows.Forms.MaskedTextBox IDLopBox;
        private System.Windows.Forms.MaskedTextBox NamHocBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox LHDT_Box;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox LT_Box;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox GVCN_Box;
        private System.Windows.Forms.MaskedTextBox LopBox;
    }
}