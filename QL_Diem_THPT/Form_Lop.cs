﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class Form_Lop : Form
    {
        qldBLL.TimLop data;
        public Form_Lop(Session session)
        {
            InitializeComponent();
            data = new qldBLL.TimLop(session);
        }

        private void SetupTable()       //Cài đặt điều kiện dữ liệu cho bảng
        {
            DataTable table = dataGridView1.DataSource as DataTable;
            table.Columns[0].ReadOnly = true;

            table.Columns[1].AllowDBNull = false;

            table.Columns[2].AllowDBNull = false;
            table.Columns[2].MaxLength = 10;

            table.Columns[3].AllowDBNull = false;
            table.Columns[3].DefaultValue = "Lớp Thường";
            table.Columns[3].MaxLength = 20;

            table.Columns[4].AllowDBNull = false;

            table.Columns[5].AllowDBNull = false;
            table.Columns[5].MaxLength = 10;

            table.Columns[6].MaxLength = 10;
        }
        /// <summary>
        /// Form
        /// </summary>
        private void From_Shown(object sender, EventArgs e)
        {
            string s = data.Search(IDLop: "-1");
            if (s == "")
            {
                dataGridView1.DataSource = data.table;
                SetupTable();
                CN_button.Enabled = false;
            }
            else
                qL_Log1.Log += "Lỗi: " + s + "\n";
        }
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (CN_button.Enabled)
            {
                object o = MessageBox.Show("Các thay đổi trên dữ liệu chưa được cập  nhật! \n Bạn có muốn lưu các thay đổi !", "Cảnh Báo !", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if ((DialogResult)o == DialogResult.Yes)
                    data.Update();
            }
        }
        /// <summary>
        /// Control
        /// </summary>
        private void TimKiem_Click(object sender, EventArgs e)
        {
            string s = data.Search((int)SoKQ.Value, 
                IDLopBox.Text, 
                TenLopBox.Text, 
                LHDT_Box.Text, 
                LopBox.Text,
                NamHocBox.Text,
                GVCN_Box.Text, 
                LT_Box.Text);
            if (s == "")
            {
                dataGridView1.DataSource = data.table;
                SetupTable();
                CN_button.Enabled = false;
                qL_Log1.Log += "--------------- Tìm được " + data.table.Rows.Count + " kết quả\n";
            }
            else
                qL_Log1.Log += "Lỗi: " + s + "\n";
        }
        private void CapNhat_Click(object sender, EventArgs e)
        {
            string s = data.Update();
            if(s == "")
            {
                CN_button.Enabled = false;
                qL_Log1.Log += "--------------- Cập nhật dữ liệu thành công\n";
            }
            else
                qL_Log1.Log += "lỗi: " + s + "\n";
        }
        private void AnHien_TimKiem_Click(object sender, EventArgs e)
        {
            if (panel1.Size.Height == 50)   // lếu đang ẩn tìm kiếm nâng cao
            {
                panel1.Size = new Size(panel1.Width, 100);
                panel2.Location = new Point(12, 29);
                dataGridView1.Location = new Point(12, 118);
                dataGridView1.Size = new Size(dataGridView1.Size.Width, Size.Height - 235);
                label3.Text = "Ẩn tìm kiếm nâng cao";
            }
            else                          // lếu đang Hiện tìm kiếm nâng cao
            {
                panel1.Size = new Size(panel1.Width, 50);
                panel2.Location = new Point(12, 75);
                dataGridView1.Location = new Point(12, 68);
                dataGridView1.Size = new Size(dataGridView1.Size.Width, Size.Height - 185);
                label3.Text = "Hiển thị tìm kiếm nâng cao";
            }
        }
        /// <summary>
        /// Data GridView
        /// </summary>
        private void data_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            int count = dataGridView1.SelectedRows.Count;
            object o = MessageBox.Show("Bạn có chắc muốn xóa " + count + " hàng! ", "Cân Nhắc", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if ((DialogResult)o == DialogResult.No)
                e.Cancel = true;
        }
        private void data_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            CN_button.Enabled = true;
        }
        private void data_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            string message = "Lỗi định dạng dữ liệu: dòng " + e.RowIndex + " cột " + e.ColumnIndex;
            message += "\n" + e.Exception.Message;
            MessageBox.Show(message, "Dữ liệu không hợp lệ");
        }
        private void data_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CN_button.Enabled = true;
        }

    }
}
