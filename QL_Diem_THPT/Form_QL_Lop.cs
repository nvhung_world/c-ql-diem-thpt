﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class Form_QL_Lop : Form
    {
        string idLop;
        PhanLopHS phanLop;
        PhanCongGV phanCong;
        TimHocSinh timHS;
        TimGiaoVien timGV;
        TimLop timLop;
        Session session;
        public Form_QL_Lop(Session session)
        {
            InitializeComponent();
            this.session = session;
            timHS = new TimHocSinh(session, "HocSinh.MaHocSinh, concat(HocSinh.HoDem, SPACE(1), HocSinh.Ten) as 'HoTen', HocSinh.KhoaHoc, HocSinh.GioiTinh");
            timGV = new TimGiaoVien(session, "MaGiaoVien, concat(HoDem, SPACE(1), Ten) as 'HoTen', BoMon");
            //...................................
            timLop = new TimLop(session, "Lop.TenLop, Lop.NamHoc, concat(GiaoVien.HoDem, SPACE(1), GiaoVien.Ten) as 'ChuNhiem', Lop.LoaiHinhDaoTao");
        }
        // Tìm kiếm lớp
        private void TimLopButton_Click(object sender, EventArgs e)
        {
            if (IDLopBox.Text == "")
                return;

            string s = timLop.Search(IDLop: IDLopBox.Text,LaythongTin_CN: true);
            if (s == "")
            {
                DataTable dt = timLop.table;
                if (dt.Rows.Count > 0)
                {
                    idLop = IDLopBox.Text;
                    label2.Text = "Tên lớp: " + dt.Rows[0][0].ToString();
                    label3.Text = "Năm Học: " + dt.Rows[0][1].ToString();
                    label4.Text = "Giáo viên chủ nhiệm: " + dt.Rows[0][2].ToString();
                    label5.Text = "Loại hình đào tạo: " + dt.Rows[0][3].ToString();
                    Text = "Quản Lý Lớp: " + dt.Rows[0][0].ToString();
                    tabControl1.Enabled = true;

                    // Lấy danh sách học sinh


                    phanLop = new PhanLopHS(session,idLop);
                    s = phanLop.Search();
                    if (s == "")
                    {
                        DS_HS.DataSource = phanLop.table;
                        qL_Log1.Log = "-------- Đọc dữ liệu thành công \n";
                    }
                    else
                        qL_Log1.Log = "Lỗi: " + s + "\n";

                    // Lấy danh sách giáo viên giảng dậy
                    phanCong = new PhanCongGV(session, idLop);
                    s = phanCong.Search();
                    if (s == "")
                    {
                        DS_GV.DataSource = phanCong.table;
                        qL_Log2.Log = "-------- Đọc dữ liệu thành công \n";
                    }
                    else
                        qL_Log2.Log = "Lỗi: " + s + "\n";
                }
            }
            else
            {
                qL_Log1.Log = "Lỗi: " + s + "\n";
                qL_Log2.Log = "Lỗi: " + s + "\n";
            }
        }
        // Quản Lý Danh sách học sinh
        private void Tim_HS_Click(object sender, EventArgs e)
        {
            string s = timHS.Search(MaHS: MaHS_Box.Text,
                HoTen: HT_HS_Box.Text,
                KhoaHoc: KhoaHox_Box.Text,
                Lop: Lop_Box.Text,
                NamHoc: NamHoc_Box.Text,
                Nam: Nam_HS.Checked,
                Nu: Nu_HS.Checked);
            if (s == "")
                dataGridView1.DataSource = timHS.table;
            else
                Console.WriteLine(s);

        }
        private void Them_HS_Click(object sender, EventArgs e)
        {
            DataRowCollection rows  = (dataGridView1.DataSource as DataTable).Rows; 
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                (DS_HS.DataSource as DataTable).Rows.Add(rows[row.Index].ItemArray);
                rows.RemoveAt(row.Index);
                CapNhat_HS.Enabled = true;
            }
        }
        private void Update_HS_Click(object sender, EventArgs e)
        {
            string s = phanLop.Update();
            if (s == "")
            {
                qL_Log1.Log += "-------- Cập nhật dữ liệu thành công\n";
                CapNhat_HS.Enabled = false;
            }
            else
                qL_Log1.Log += "Lỗi: " + s + "\n";
        }

        // Quản Lý Danh sách giáo viên
        private void Tim_GV_Click(object sender, EventArgs e)
        {
            string s = timGV.Search(MaGV: MaGV_Box.Text, 
                HoTen: HT_GV_Box.Text, 
                BoMon: BoMon_Box.Text,
                Nam: Nam_GV.Checked,
                Nu: Nu_GV.Checked);
            if (s == "")
                dataGridView4.DataSource = timGV.table;
            else
                Console.WriteLine(s);

        }
        private void Them_GV_Click(object sender, EventArgs e)
        {
            DataRowCollection rows = (dataGridView4.DataSource as DataTable).Rows;
            foreach (DataGridViewRow row in dataGridView4.SelectedRows)
            {
                (DS_GV.DataSource as DataTable).Rows.Add(rows[row.Index].ItemArray);
                rows.RemoveAt(row.Index);
                CapNhatGV_Button.Enabled = true;
            }
        }
        private void Update_GV_Click(object sender, EventArgs e)
        {
            string s = phanCong.Update();
            if (s == "")
            {
                qL_Log2.Log += "-------- Cập nhật dữ liệu thành công\n";
                CapNhatGV_Button.Enabled = false;
            }
            else
                qL_Log2.Log += "Lỗi: " + s + "\n";
        }

        /// <summary>
        /// 
        /// </summary>
        private void data_HS_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            int count = DS_HS.SelectedRows.Count;
            object o = MessageBox.Show("Bạn có chắc muốn xóa " + count + " hàng! ", "Cân Nhắc", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if ((DialogResult)o == DialogResult.No)
                e.Cancel = true;
        }
        private void data_HS_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            CapNhat_HS.Enabled = true;
        }
        //
        private void data_GV_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            int count = DS_GV.SelectedRows.Count;
            object o = MessageBox.Show("Bạn có chắc muốn xóa " + count + " hàng! ", "Cân Nhắc", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if ((DialogResult)o == DialogResult.No)
                e.Cancel = true;
        }
        private void data_GV_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            CapNhatGV_Button.Enabled = true;
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if(CapNhat_HS.Enabled)
            {
                object o = MessageBox.Show("Các thay đổi trên dữ liệu chưa được cập  nhật! \n Bạn có muốn lưu các thay đổi !", "Cảnh Báo !", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if ((DialogResult)o == DialogResult.Yes)
                    phanLop.Update();
            }
            if(CapNhatGV_Button.Enabled)
            {
                object o = MessageBox.Show("Các thay đổi trên dữ liệu chưa được cập  nhật! \n Bạn có muốn lưu các thay đổi !", "Cảnh Báo !", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if ((DialogResult)o == DialogResult.Yes)
                    phanCong.Update();
            }
        }


    }
}
