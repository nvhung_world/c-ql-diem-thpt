﻿namespace QL_Diem
{
    partial class Form_HocSinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MaHS_Box = new System.Windows.Forms.TextBox();
            this.TK_button = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.L_Box = new System.Windows.Forms.TextBox();
            this.SoDT_Box = new System.Windows.Forms.MaskedTextBox();
            this.K_Box = new System.Windows.Forms.MaskedTextBox();
            this.Nu = new System.Windows.Forms.CheckBox();
            this.Nam = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.NS_Box = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DC_Box = new System.Windows.Forms.TextBox();
            this.E_Box = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SoKQ = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.HT_Box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CN_button = new System.Windows.Forms.Button();
            this.qL_Log1 = new QL_Diem.QL_Log();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoKQ)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Số Kết Quả";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(209, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Mã HocSinh";
            // 
            // MaHS_Box
            // 
            this.MaHS_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.MaHS_Box.Location = new System.Drawing.Point(286, 3);
            this.MaHS_Box.MaxLength = 10;
            this.MaHS_Box.Name = "MaHS_Box";
            this.MaHS_Box.Size = new System.Drawing.Size(116, 20);
            this.MaHS_Box.TabIndex = 2;
            // 
            // TK_button
            // 
            this.TK_button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TK_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.TK_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.TK_button.FlatAppearance.BorderSize = 0;
            this.TK_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TK_button.Location = new System.Drawing.Point(788, 3);
            this.TK_button.Name = "TK_button";
            this.TK_button.Size = new System.Drawing.Size(107, 44);
            this.TK_button.TabIndex = 1;
            this.TK_button.Text = "Tìm Kiếm";
            this.TK_button.UseVisualStyleBackColor = false;
            this.TK_button.Click += new System.EventHandler(this.TimKiem_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.L_Box);
            this.panel2.Controls.Add(this.SoDT_Box);
            this.panel2.Controls.Add(this.K_Box);
            this.panel2.Controls.Add(this.Nu);
            this.panel2.Controls.Add(this.Nam);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.NS_Box);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DC_Box);
            this.panel2.Controls.Add(this.E_Box);
            this.panel2.Location = new System.Drawing.Point(3, 75);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(779, 56);
            this.panel2.TabIndex = 0;
            // 
            // L_Box
            // 
            this.L_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.L_Box.Location = new System.Drawing.Point(95, 29);
            this.L_Box.MaxLength = 10;
            this.L_Box.Name = "L_Box";
            this.L_Box.Size = new System.Drawing.Size(100, 20);
            this.L_Box.TabIndex = 10;
            // 
            // SoDT_Box
            // 
            this.SoDT_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.SoDT_Box.Location = new System.Drawing.Point(327, 28);
            this.SoDT_Box.Mask = "00000000000";
            this.SoDT_Box.Name = "SoDT_Box";
            this.SoDT_Box.Size = new System.Drawing.Size(116, 20);
            this.SoDT_Box.TabIndex = 19;
            // 
            // K_Box
            // 
            this.K_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.K_Box.Location = new System.Drawing.Point(95, 3);
            this.K_Box.Mask = "000000";
            this.K_Box.Name = "K_Box";
            this.K_Box.Size = new System.Drawing.Size(100, 20);
            this.K_Box.TabIndex = 17;
            // 
            // Nu
            // 
            this.Nu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Nu.AutoSize = true;
            this.Nu.Location = new System.Drawing.Point(681, 30);
            this.Nu.Name = "Nu";
            this.Nu.Size = new System.Drawing.Size(40, 17);
            this.Nu.TabIndex = 16;
            this.Nu.Text = "Nữ";
            this.Nu.UseVisualStyleBackColor = true;
            this.Nu.Click += new System.EventHandler(this.GioiTinh_Changed);
            // 
            // Nam
            // 
            this.Nam.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Nam.AutoSize = true;
            this.Nam.Location = new System.Drawing.Point(681, 5);
            this.Nam.Name = "Nam";
            this.Nam.Size = new System.Drawing.Size(48, 17);
            this.Nam.TabIndex = 15;
            this.Nam.Text = "Nam";
            this.Nam.UseVisualStyleBackColor = true;
            this.Nam.Click += new System.EventHandler(this.GioiTinh_Changed);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(246, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Ngày Sinh";
            // 
            // NS_Box
            // 
            this.NS_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.NS_Box.Location = new System.Drawing.Point(327, 3);
            this.NS_Box.Mask = "00-00-0000";
            this.NS_Box.Name = "NS_Box";
            this.NS_Box.Size = new System.Drawing.Size(116, 20);
            this.NS_Box.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(478, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Địa Chỉ";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(478, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Email";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(246, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Số Điện Thoại";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Lớp";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Khóa";
            // 
            // DC_Box
            // 
            this.DC_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DC_Box.Location = new System.Drawing.Point(525, 3);
            this.DC_Box.MaxLength = 60;
            this.DC_Box.Name = "DC_Box";
            this.DC_Box.Size = new System.Drawing.Size(116, 20);
            this.DC_Box.TabIndex = 10;
            // 
            // E_Box
            // 
            this.E_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.E_Box.Location = new System.Drawing.Point(525, 28);
            this.E_Box.MaxLength = 40;
            this.E_Box.Name = "E_Box";
            this.E_Box.Size = new System.Drawing.Size(116, 20);
            this.E_Box.TabIndex = 10;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 73);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(910, 259);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.data_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.data_DataError);
            this.dataGridView1.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.data_UserDeletedRow);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.data_UserDeletingRow);
            // 
            // SoKQ
            // 
            this.SoKQ.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.SoKQ.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.SoKQ.Location = new System.Drawing.Point(108, 3);
            this.SoKQ.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.SoKQ.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.SoKQ.Name = "SoKQ";
            this.SoKQ.Size = new System.Drawing.Size(78, 20);
            this.SoKQ.TabIndex = 6;
            this.SoKQ.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.HT_Box);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.SoKQ);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.MaHS_Box);
            this.panel1.Controls.Add(this.TK_button);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(910, 50);
            this.panel1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(431, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Họ Tên";
            // 
            // HT_Box
            // 
            this.HT_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.HT_Box.Location = new System.Drawing.Point(480, 3);
            this.HT_Box.MaxLength = 40;
            this.HT_Box.Name = "HT_Box";
            this.HT_Box.Size = new System.Drawing.Size(262, 20);
            this.HT_Box.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(338, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Hiển thị tìm kiếm nâng cao";
            this.label3.Click += new System.EventHandler(this.AnHien_TimKiem_Click);
            // 
            // CN_button
            // 
            this.CN_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CN_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.CN_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.CN_button.Enabled = false;
            this.CN_button.FlatAppearance.BorderSize = 0;
            this.CN_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CN_button.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CN_button.Location = new System.Drawing.Point(778, 338);
            this.CN_button.Name = "CN_button";
            this.CN_button.Size = new System.Drawing.Size(144, 60);
            this.CN_button.TabIndex = 8;
            this.CN_button.Text = "Cập Nhật";
            this.CN_button.UseVisualStyleBackColor = false;
            this.CN_button.Click += new System.EventHandler(this.CapNhat_Click);
            // 
            // qL_Log1
            // 
            this.qL_Log1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qL_Log1.fromName = "Quản Lý Học Sinh";
            this.qL_Log1.Location = new System.Drawing.Point(12, 338);
            this.qL_Log1.Log = "";
            this.qL_Log1.Name = "qL_Log1";
            this.qL_Log1.Size = new System.Drawing.Size(760, 60);
            this.qL_Log1.TabIndex = 10;
            // 
            // Form_HocSinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(934, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.qL_Log1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.CN_button);
            this.MinimumSize = new System.Drawing.Size(950, 450);
            this.Name = "Form_HocSinh";
            this.Text = "Quản Lý Học Sinh";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Shown += new System.EventHandler(this.Form_Shown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoKQ)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private QL_Log qL_Log1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MaHS_Box;
        private System.Windows.Forms.Button TK_button;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox Nu;
        private System.Windows.Forms.CheckBox Nam;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox NS_Box;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DC_Box;
        private System.Windows.Forms.TextBox E_Box;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.NumericUpDown SoKQ;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox HT_Box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CN_button;
        private System.Windows.Forms.MaskedTextBox K_Box;
        private System.Windows.Forms.TextBox L_Box;
        private System.Windows.Forms.MaskedTextBox SoDT_Box;
    }
}