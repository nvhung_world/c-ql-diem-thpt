﻿namespace QL_Diem
{
    partial class Form_QL_Lop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TimLop_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IDLopBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.ThemHS_Botton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CapNhat_HS = new System.Windows.Forms.Button();
            this.qL_Log1 = new QL_Diem.QL_Log();
            this.DS_HS = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.Lop_Box = new System.Windows.Forms.TextBox();
            this.NamHoc_Box = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Nu_HS = new System.Windows.Forms.CheckBox();
            this.Nam_HS = new System.Windows.Forms.CheckBox();
            this.KhoaHox_Box = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.HT_HS_Box = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.MaHS_Box = new System.Windows.Forms.TextBox();
            this.TimHS_Button = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.ThemGV_Button = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.CapNhatGV_Button = new System.Windows.Forms.Button();
            this.qL_Log2 = new QL_Diem.QL_Log();
            this.DS_GV = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.BoMon_Box = new System.Windows.Forms.TextBox();
            this.Nu_GV = new System.Windows.Forms.CheckBox();
            this.Nam_GV = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.HT_GV_Box = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.MaGV_Box = new System.Windows.Forms.TextBox();
            this.TimGV_Button = new System.Windows.Forms.Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DS_HS)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DS_GV)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // TimLop_Button
            // 
            this.TimLop_Button.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TimLop_Button.Location = new System.Drawing.Point(169, 10);
            this.TimLop_Button.Name = "TimLop_Button";
            this.TimLop_Button.Size = new System.Drawing.Size(85, 23);
            this.TimLop_Button.TabIndex = 1;
            this.TimLop_Button.Text = "Tìm Lớp";
            this.TimLop_Button.UseVisualStyleBackColor = true;
            this.TimLop_Button.Click += new System.EventHandler(this.TimLopButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID Lớp";
            // 
            // IDLopBox
            // 
            this.IDLopBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.IDLopBox.Location = new System.Drawing.Point(51, 12);
            this.IDLopBox.Mask = "0000000000";
            this.IDLopBox.Name = "IDLopBox";
            this.IDLopBox.Size = new System.Drawing.Size(98, 20);
            this.IDLopBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(288, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tên lớp : 10A12";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(398, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Năm Học: 9999";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(509, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Giáo viên chủ nhiệm: Nguyễn Văn Hung";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(738, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Loại hình đào tạo : Lớp Chuyên";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Enabled = false;
            this.tabControl1.Location = new System.Drawing.Point(12, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(910, 360);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.ThemHS_Botton);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(902, 334);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh Sách Học Sinh";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(650, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Danh Sách Học Sinh";
            // 
            // ThemHS_Botton
            // 
            this.ThemHS_Botton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ThemHS_Botton.Location = new System.Drawing.Point(412, 133);
            this.ThemHS_Botton.Name = "ThemHS_Botton";
            this.ThemHS_Botton.Size = new System.Drawing.Size(84, 48);
            this.ThemHS_Botton.TabIndex = 2;
            this.ThemHS_Botton.Text = "Thêm --------->";
            this.ThemHS_Botton.UseVisualStyleBackColor = true;
            this.ThemHS_Botton.Click += new System.EventHandler(this.Them_HS_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.CapNhat_HS);
            this.panel2.Controls.Add(this.qL_Log1);
            this.panel2.Controls.Add(this.DS_HS);
            this.panel2.Location = new System.Drawing.Point(502, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 310);
            this.panel2.TabIndex = 1;
            // 
            // CapNhat_HS
            // 
            this.CapNhat_HS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CapNhat_HS.Enabled = false;
            this.CapNhat_HS.Location = new System.Drawing.Point(300, 258);
            this.CapNhat_HS.Name = "CapNhat_HS";
            this.CapNhat_HS.Size = new System.Drawing.Size(97, 49);
            this.CapNhat_HS.TabIndex = 2;
            this.CapNhat_HS.Text = "Cập Nhật";
            this.CapNhat_HS.UseVisualStyleBackColor = true;
            this.CapNhat_HS.Click += new System.EventHandler(this.Update_HS_Click);
            // 
            // qL_Log1
            // 
            this.qL_Log1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qL_Log1.fromName = "Quản lý học sinh trong lớp";
            this.qL_Log1.Location = new System.Drawing.Point(4, 258);
            this.qL_Log1.Log = "";
            this.qL_Log1.Name = "qL_Log1";
            this.qL_Log1.Size = new System.Drawing.Size(289, 49);
            this.qL_Log1.TabIndex = 1;
            // 
            // DS_HS
            // 
            this.DS_HS.AllowUserToAddRows = false;
            this.DS_HS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DS_HS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DS_HS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DS_HS.Location = new System.Drawing.Point(4, 4);
            this.DS_HS.Name = "DS_HS";
            this.DS_HS.ReadOnly = true;
            this.DS_HS.Size = new System.Drawing.Size(393, 248);
            this.DS_HS.TabIndex = 0;
            this.DS_HS.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.data_HS_UserDeletedRow);
            this.DS_HS.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.data_HS_UserDeletingRow);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.Lop_Box);
            this.panel1.Controls.Add(this.NamHoc_Box);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.Nu_HS);
            this.panel1.Controls.Add(this.Nam_HS);
            this.panel1.Controls.Add(this.KhoaHox_Box);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.HT_HS_Box);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.MaHS_Box);
            this.panel1.Controls.Add(this.TimHS_Button);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(406, 334);
            this.panel1.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Tên lớp";
            // 
            // Lop_Box
            // 
            this.Lop_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Lop_Box.Location = new System.Drawing.Point(53, 62);
            this.Lop_Box.MaxLength = 10;
            this.Lop_Box.Name = "Lop_Box";
            this.Lop_Box.Size = new System.Drawing.Size(77, 20);
            this.Lop_Box.TabIndex = 13;
            // 
            // NamHoc_Box
            // 
            this.NamHoc_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.NamHoc_Box.Location = new System.Drawing.Point(218, 62);
            this.NamHoc_Box.Mask = "000000";
            this.NamHoc_Box.Name = "NamHoc_Box";
            this.NamHoc_Box.Size = new System.Drawing.Size(77, 20);
            this.NamHoc_Box.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(160, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Năm Học";
            // 
            // Nu_HS
            // 
            this.Nu_HS.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Nu_HS.AutoSize = true;
            this.Nu_HS.Location = new System.Drawing.Point(232, 31);
            this.Nu_HS.Name = "Nu_HS";
            this.Nu_HS.Size = new System.Drawing.Size(40, 17);
            this.Nu_HS.TabIndex = 10;
            this.Nu_HS.Text = "Nữ";
            this.Nu_HS.UseVisualStyleBackColor = true;
            // 
            // Nam_HS
            // 
            this.Nam_HS.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Nam_HS.AutoSize = true;
            this.Nam_HS.Location = new System.Drawing.Point(178, 32);
            this.Nam_HS.Name = "Nam_HS";
            this.Nam_HS.Size = new System.Drawing.Size(48, 17);
            this.Nam_HS.TabIndex = 9;
            this.Nam_HS.Text = "Nam";
            this.Nam_HS.UseVisualStyleBackColor = true;
            // 
            // KhoaHox_Box
            // 
            this.KhoaHox_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.KhoaHox_Box.Location = new System.Drawing.Point(53, 29);
            this.KhoaHox_Box.Mask = "000000";
            this.KhoaHox_Box.Name = "KhoaHox_Box";
            this.KhoaHox_Box.Size = new System.Drawing.Size(77, 20);
            this.KhoaHox_Box.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Khóa";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(175, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Họ tên";
            // 
            // HT_HS_Box
            // 
            this.HT_HS_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.HT_HS_Box.Location = new System.Drawing.Point(218, 3);
            this.HT_HS_Box.MaxLength = 40;
            this.HT_HS_Box.Name = "HT_HS_Box";
            this.HT_HS_Box.Size = new System.Drawing.Size(167, 20);
            this.HT_HS_Box.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Mã học sinh";
            // 
            // MaHS_Box
            // 
            this.MaHS_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.MaHS_Box.Location = new System.Drawing.Point(77, 3);
            this.MaHS_Box.MaxLength = 10;
            this.MaHS_Box.Name = "MaHS_Box";
            this.MaHS_Box.Size = new System.Drawing.Size(79, 20);
            this.MaHS_Box.TabIndex = 2;
            // 
            // TimHS_Button
            // 
            this.TimHS_Button.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TimHS_Button.Location = new System.Drawing.Point(325, 29);
            this.TimHS_Button.Name = "TimHS_Button";
            this.TimHS_Button.Size = new System.Drawing.Size(75, 54);
            this.TimHS_Button.TabIndex = 1;
            this.TimHS_Button.Text = "Tìm kiếm";
            this.TimHS_Button.UseVisualStyleBackColor = true;
            this.TimHS_Button.Click += new System.EventHandler(this.Tim_HS_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 89);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(400, 242);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.ThemGV_Button);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(902, 334);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Phân Công Giảng Dậy";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(632, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(163, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Danh Sách Giáo Viên Giảng Dậy";
            // 
            // ThemGV_Button
            // 
            this.ThemGV_Button.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ThemGV_Button.Location = new System.Drawing.Point(412, 133);
            this.ThemGV_Button.Name = "ThemGV_Button";
            this.ThemGV_Button.Size = new System.Drawing.Size(84, 48);
            this.ThemGV_Button.TabIndex = 5;
            this.ThemGV_Button.Text = "Thêm --------->";
            this.ThemGV_Button.UseVisualStyleBackColor = true;
            this.ThemGV_Button.Click += new System.EventHandler(this.Them_GV_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.CapNhatGV_Button);
            this.panel3.Controls.Add(this.qL_Log2);
            this.panel3.Controls.Add(this.DS_GV);
            this.panel3.Location = new System.Drawing.Point(502, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(400, 310);
            this.panel3.TabIndex = 4;
            // 
            // CapNhatGV_Button
            // 
            this.CapNhatGV_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CapNhatGV_Button.Enabled = false;
            this.CapNhatGV_Button.Location = new System.Drawing.Point(300, 258);
            this.CapNhatGV_Button.Name = "CapNhatGV_Button";
            this.CapNhatGV_Button.Size = new System.Drawing.Size(97, 49);
            this.CapNhatGV_Button.TabIndex = 2;
            this.CapNhatGV_Button.Text = "Cập Nhật";
            this.CapNhatGV_Button.UseVisualStyleBackColor = true;
            this.CapNhatGV_Button.Click += new System.EventHandler(this.Update_GV_Click);
            // 
            // qL_Log2
            // 
            this.qL_Log2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qL_Log2.fromName = "Quản Lý giáo viên giảng dậy";
            this.qL_Log2.Location = new System.Drawing.Point(4, 258);
            this.qL_Log2.Log = "";
            this.qL_Log2.Name = "qL_Log2";
            this.qL_Log2.Size = new System.Drawing.Size(289, 49);
            this.qL_Log2.TabIndex = 1;
            // 
            // DS_GV
            // 
            this.DS_GV.AllowUserToAddRows = false;
            this.DS_GV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DS_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DS_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DS_GV.Location = new System.Drawing.Point(4, 4);
            this.DS_GV.Name = "DS_GV";
            this.DS_GV.ReadOnly = true;
            this.DS_GV.Size = new System.Drawing.Size(393, 248);
            this.DS_GV.TabIndex = 0;
            this.DS_GV.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.data_GV_UserDeletedRow);
            this.DS_GV.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.data_GV_UserDeletingRow);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.BoMon_Box);
            this.panel4.Controls.Add(this.Nu_GV);
            this.panel4.Controls.Add(this.Nam_GV);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.HT_GV_Box);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.MaGV_Box);
            this.panel4.Controls.Add(this.TimGV_Button);
            this.panel4.Controls.Add(this.dataGridView4);
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(406, 334);
            this.panel4.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Bộ Môn";
            // 
            // BoMon_Box
            // 
            this.BoMon_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BoMon_Box.Location = new System.Drawing.Point(51, 32);
            this.BoMon_Box.MaxLength = 10;
            this.BoMon_Box.Name = "BoMon_Box";
            this.BoMon_Box.Size = new System.Drawing.Size(77, 20);
            this.BoMon_Box.TabIndex = 13;
            // 
            // Nu_GV
            // 
            this.Nu_GV.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Nu_GV.AutoSize = true;
            this.Nu_GV.Location = new System.Drawing.Point(227, 34);
            this.Nu_GV.Name = "Nu_GV";
            this.Nu_GV.Size = new System.Drawing.Size(40, 17);
            this.Nu_GV.TabIndex = 10;
            this.Nu_GV.Text = "Nữ";
            this.Nu_GV.UseVisualStyleBackColor = true;
            // 
            // Nam_GV
            // 
            this.Nam_GV.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Nam_GV.AutoSize = true;
            this.Nam_GV.Location = new System.Drawing.Point(173, 35);
            this.Nam_GV.Name = "Nam_GV";
            this.Nam_GV.Size = new System.Drawing.Size(48, 17);
            this.Nam_GV.TabIndex = 9;
            this.Nam_GV.Text = "Nam";
            this.Nam_GV.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(175, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Họ tên";
            // 
            // HT_GV_Box
            // 
            this.HT_GV_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.HT_GV_Box.Location = new System.Drawing.Point(218, 3);
            this.HT_GV_Box.MaxLength = 40;
            this.HT_GV_Box.Name = "HT_GV_Box";
            this.HT_GV_Box.Size = new System.Drawing.Size(167, 20);
            this.HT_GV_Box.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Mã giáo viên";
            // 
            // MaGV_Box
            // 
            this.MaGV_Box.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.MaGV_Box.Location = new System.Drawing.Point(77, 3);
            this.MaGV_Box.MaxLength = 10;
            this.MaGV_Box.Name = "MaGV_Box";
            this.MaGV_Box.Size = new System.Drawing.Size(79, 20);
            this.MaGV_Box.TabIndex = 2;
            // 
            // TimGV_Button
            // 
            this.TimGV_Button.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.TimGV_Button.Location = new System.Drawing.Point(293, 29);
            this.TimGV_Button.Name = "TimGV_Button";
            this.TimGV_Button.Size = new System.Drawing.Size(110, 24);
            this.TimGV_Button.TabIndex = 1;
            this.TimGV_Button.Text = "Tìm kiếm";
            this.TimGV_Button.UseVisualStyleBackColor = true;
            this.TimGV_Button.Click += new System.EventHandler(this.Tim_GV_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(3, 59);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.Size = new System.Drawing.Size(400, 272);
            this.dataGridView4.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(412, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Chọn rồi nhấn";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(412, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Chọn rồi nhấn";
            // 
            // Form_QL_Lop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 411);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IDLopBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TimLop_Button);
            this.MinimumSize = new System.Drawing.Size(950, 450);
            this.Name = "Form_QL_Lop";
            this.Text = "Quản Lý Lớp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DS_HS)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DS_GV)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button TimLop_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox IDLopBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button ThemHS_Botton;
        private System.Windows.Forms.MaskedTextBox KhoaHox_Box;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox HT_HS_Box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox MaHS_Box;
        private System.Windows.Forms.Button TimHS_Button;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Lop_Box;
        private System.Windows.Forms.MaskedTextBox NamHoc_Box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox Nu_HS;
        private System.Windows.Forms.CheckBox Nam_HS;
        private System.Windows.Forms.Button CapNhat_HS;
        private QL_Log qL_Log1;
        private System.Windows.Forms.DataGridView DS_HS;
        private System.Windows.Forms.Button ThemGV_Button;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button CapNhatGV_Button;
        private QL_Log qL_Log2;
        private System.Windows.Forms.DataGridView DS_GV;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox BoMon_Box;
        private System.Windows.Forms.CheckBox Nu_GV;
        private System.Windows.Forms.CheckBox Nam_GV;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox HT_GV_Box;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox MaGV_Box;
        private System.Windows.Forms.Button TimGV_Button;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
    }
}