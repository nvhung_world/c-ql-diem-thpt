﻿namespace QL_Diem
{
    partial class AccInfomation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaGV = new System.Windows.Forms.Label();
            this.IDM = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.HoDem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Ten = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SoCMt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NVT = new System.Windows.Forms.MaskedTextBox();
            this.NRT = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DiaChi = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Sdt = new System.Windows.Forms.TextBox();
            this.Nam = new System.Windows.Forms.CheckBox();
            this.Nu = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ngaySinh = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // MaGV
            // 
            this.MaGV.AutoSize = true;
            this.MaGV.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.MaGV.Location = new System.Drawing.Point(65, 16);
            this.MaGV.Name = "MaGV";
            this.MaGV.Size = new System.Drawing.Size(42, 15);
            this.MaGV.TabIndex = 0;
            this.MaGV.Text = "MaGV";
            // 
            // IDM
            // 
            this.IDM.AutoSize = true;
            this.IDM.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.IDM.Location = new System.Drawing.Point(65, 46);
            this.IDM.Name = "IDM";
            this.IDM.Size = new System.Drawing.Size(59, 15);
            this.IDM.TabIndex = 1;
            this.IDM.Text = "Bộ Môn : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Họ đệm";
            // 
            // HoDem
            // 
            this.HoDem.Location = new System.Drawing.Point(139, 80);
            this.HoDem.MaxLength = 30;
            this.HoDem.Name = "HoDem";
            this.HoDem.Size = new System.Drawing.Size(196, 20);
            this.HoDem.TabIndex = 3;
            this.HoDem.TextChanged += new System.EventHandler(this.Text_Changed);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tên";
            // 
            // Ten
            // 
            this.Ten.Location = new System.Drawing.Point(139, 106);
            this.Ten.MaxLength = 10;
            this.Ten.Name = "Ten";
            this.Ten.Size = new System.Drawing.Size(196, 20);
            this.Ten.TabIndex = 5;
            this.Ten.TextChanged += new System.EventHandler(this.Text_Changed);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số Cmt";
            // 
            // SoCMt
            // 
            this.SoCMt.Location = new System.Drawing.Point(139, 132);
            this.SoCMt.MaxLength = 15;
            this.SoCMt.Name = "SoCMt";
            this.SoCMt.ReadOnly = true;
            this.SoCMt.Size = new System.Drawing.Size(196, 20);
            this.SoCMt.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ngày Sinh";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Giới Tính";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Ngày vào Trường";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Ngày rời trường";
            // 
            // NVT
            // 
            this.NVT.Location = new System.Drawing.Point(139, 210);
            this.NVT.Mask = "00-00-0000";
            this.NVT.Name = "NVT";
            this.NVT.ReadOnly = true;
            this.NVT.Size = new System.Drawing.Size(196, 20);
            this.NVT.TabIndex = 15;
            // 
            // NRT
            // 
            this.NRT.Location = new System.Drawing.Point(139, 236);
            this.NRT.Mask = "00-00-0000";
            this.NRT.Name = "NRT";
            this.NRT.ReadOnly = true;
            this.NRT.Size = new System.Drawing.Size(196, 20);
            this.NRT.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Địa chỉ";
            // 
            // DiaChi
            // 
            this.DiaChi.Location = new System.Drawing.Point(138, 262);
            this.DiaChi.MaxLength = 60;
            this.DiaChi.Name = "DiaChi";
            this.DiaChi.Size = new System.Drawing.Size(197, 20);
            this.DiaChi.TabIndex = 18;
            this.DiaChi.TextChanged += new System.EventHandler(this.Text_Changed);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 291);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Email";
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(138, 288);
            this.Email.MaxLength = 40;
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(197, 20);
            this.Email.TabIndex = 20;
            this.Email.TextChanged += new System.EventHandler(this.Text_Changed);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 317);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Sdt";
            // 
            // Sdt
            // 
            this.Sdt.Location = new System.Drawing.Point(138, 314);
            this.Sdt.MaxLength = 11;
            this.Sdt.Name = "Sdt";
            this.Sdt.Size = new System.Drawing.Size(197, 20);
            this.Sdt.TabIndex = 22;
            this.Sdt.TextChanged += new System.EventHandler(this.Text_Changed);
            // 
            // Nam
            // 
            this.Nam.AutoSize = true;
            this.Nam.Location = new System.Drawing.Point(139, 187);
            this.Nam.Name = "Nam";
            this.Nam.Size = new System.Drawing.Size(48, 17);
            this.Nam.TabIndex = 0;
            this.Nam.Text = "Nam";
            this.Nam.UseVisualStyleBackColor = true;
            this.Nam.Click += new System.EventHandler(this.GT_Click);
            // 
            // Nu
            // 
            this.Nu.AutoSize = true;
            this.Nu.Location = new System.Drawing.Point(236, 187);
            this.Nu.Name = "Nu";
            this.Nu.Size = new System.Drawing.Size(40, 17);
            this.Nu.TabIndex = 1;
            this.Nu.Text = "Nữ";
            this.Nu.UseVisualStyleBackColor = true;
            this.Nu.Click += new System.EventHandler(this.GT_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button1.Location = new System.Drawing.Point(248, 340);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 39);
            this.button1.TabIndex = 23;
            this.button1.Text = "Lưu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(52, 354);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 24;
            // 
            // ngaySinh
            // 
            this.ngaySinh.Location = new System.Drawing.Point(139, 158);
            this.ngaySinh.MaxLength = 10;
            this.ngaySinh.Name = "ngaySinh";
            this.ngaySinh.Size = new System.Drawing.Size(196, 20);
            this.ngaySinh.TabIndex = 25;
            // 
            // AccInfomation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 391);
            this.Controls.Add(this.ngaySinh);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Nam);
            this.Controls.Add(this.Nu);
            this.Controls.Add(this.Sdt);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DiaChi);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.NRT);
            this.Controls.Add(this.NVT);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SoCMt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ten);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HoDem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDM);
            this.Controls.Add(this.MaGV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccInfomation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin giáo viên";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MaGV;
        private System.Windows.Forms.Label IDM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox HoDem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Ten;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SoCMt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox NVT;
        private System.Windows.Forms.MaskedTextBox NRT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox DiaChi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Email;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Sdt;
        private System.Windows.Forms.CheckBox Nu;
        private System.Windows.Forms.CheckBox Nam;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ngaySinh;
    }
}