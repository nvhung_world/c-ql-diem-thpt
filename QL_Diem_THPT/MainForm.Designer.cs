﻿namespace QL_Diem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thongTinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taiKhoanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dangXuatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thoatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quanTriVienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qtHocSinhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qtGiaoVienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qtLopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSachLopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSachToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qlKhoaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caiDatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuanLyHocSinhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NhapDiemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TheoDoiHocTapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.quanTriVienToolStripMenuItem,
            this.QuanLyHocSinhToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thongTinToolStripMenuItem,
            this.taiKhoanToolStripMenuItem,
            this.dangXuatToolStripMenuItem,
            this.thoatToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // thongTinToolStripMenuItem
            // 
            this.thongTinToolStripMenuItem.Name = "thongTinToolStripMenuItem";
            this.thongTinToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.thongTinToolStripMenuItem.Text = "Thông Tin Cá Nhân";
            this.thongTinToolStripMenuItem.ToolTipText = "TTCN";
            this.thongTinToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // taiKhoanToolStripMenuItem
            // 
            this.taiKhoanToolStripMenuItem.Name = "taiKhoanToolStripMenuItem";
            this.taiKhoanToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.taiKhoanToolStripMenuItem.Text = "Bảo Vệ Tài Khoản";
            this.taiKhoanToolStripMenuItem.ToolTipText = "BVTK";
            this.taiKhoanToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // dangXuatToolStripMenuItem
            // 
            this.dangXuatToolStripMenuItem.Name = "dangXuatToolStripMenuItem";
            this.dangXuatToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.dangXuatToolStripMenuItem.Text = "Đăng Xuất";
            this.dangXuatToolStripMenuItem.Click += new System.EventHandler(this.dangXuatToolStripMenuItem_Click);
            // 
            // thoatToolStripMenuItem
            // 
            this.thoatToolStripMenuItem.Name = "thoatToolStripMenuItem";
            this.thoatToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.thoatToolStripMenuItem.Text = "Thoát";
            this.thoatToolStripMenuItem.Click += new System.EventHandler(this.thoatToolStripMenuItem_Click);
            // 
            // quanTriVienToolStripMenuItem
            // 
            this.quanTriVienToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.qtHocSinhToolStripMenuItem,
            this.qtGiaoVienToolStripMenuItem,
            this.qtLopToolStripMenuItem,
            this.qlKhoaToolStripMenuItem,
            this.caiDatToolStripMenuItem});
            this.quanTriVienToolStripMenuItem.Name = "quanTriVienToolStripMenuItem";
            this.quanTriVienToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.quanTriVienToolStripMenuItem.Text = "Quản Trị Hệ Thống";
            // 
            // qtHocSinhToolStripMenuItem
            // 
            this.qtHocSinhToolStripMenuItem.Name = "qtHocSinhToolStripMenuItem";
            this.qtHocSinhToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.qtHocSinhToolStripMenuItem.Text = "Học Sinh";
            this.qtHocSinhToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // qtGiaoVienToolStripMenuItem
            // 
            this.qtGiaoVienToolStripMenuItem.Name = "qtGiaoVienToolStripMenuItem";
            this.qtGiaoVienToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.qtGiaoVienToolStripMenuItem.Text = "Giáo Viên";
            this.qtGiaoVienToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // qtLopToolStripMenuItem
            // 
            this.qtLopToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.danhSachLopToolStripMenuItem,
            this.danhSachToolStripMenuItem});
            this.qtLopToolStripMenuItem.Name = "qtLopToolStripMenuItem";
            this.qtLopToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.qtLopToolStripMenuItem.Text = "Lớp";
            // 
            // danhSachLopToolStripMenuItem
            // 
            this.danhSachLopToolStripMenuItem.Name = "danhSachLopToolStripMenuItem";
            this.danhSachLopToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.danhSachLopToolStripMenuItem.Text = "Danh Sách Lớp";
            this.danhSachLopToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // danhSachToolStripMenuItem
            // 
            this.danhSachToolStripMenuItem.Name = "danhSachToolStripMenuItem";
            this.danhSachToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.danhSachToolStripMenuItem.Text = "Quản Lý Lớp";
            this.danhSachToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // qlKhoaToolStripMenuItem
            // 
            this.qlKhoaToolStripMenuItem.Name = "qlKhoaToolStripMenuItem";
            this.qlKhoaToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.qlKhoaToolStripMenuItem.Text = "Bộ Môn";
            this.qlKhoaToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // caiDatToolStripMenuItem
            // 
            this.caiDatToolStripMenuItem.Name = "caiDatToolStripMenuItem";
            this.caiDatToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.caiDatToolStripMenuItem.Text = "Cài Đặt";
            // 
            // QuanLyHocSinhToolStripMenuItem
            // 
            this.QuanLyHocSinhToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NhapDiemToolStripMenuItem,
            this.TheoDoiHocTapToolStripMenuItem});
            this.QuanLyHocSinhToolStripMenuItem.Name = "QuanLyHocSinhToolStripMenuItem";
            this.QuanLyHocSinhToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
            this.QuanLyHocSinhToolStripMenuItem.Text = "Quản Lý Học Sinh";
            // 
            // NhapDiemToolStripMenuItem
            // 
            this.NhapDiemToolStripMenuItem.Name = "NhapDiemToolStripMenuItem";
            this.NhapDiemToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.NhapDiemToolStripMenuItem.Text = "Nhập Điểm";
            this.NhapDiemToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // TheoDoiHocTapToolStripMenuItem
            // 
            this.TheoDoiHocTapToolStripMenuItem.Name = "TheoDoiHocTapToolStripMenuItem";
            this.TheoDoiHocTapToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.TheoDoiHocTapToolStripMenuItem.Text = "Theo Dõi Học Tập";
            this.TheoDoiHocTapToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản Lý Điểm THPT";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thongTinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taiKhoanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dangXuatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thoatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quanTriVienToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qtHocSinhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qtGiaoVienToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qlKhoaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qtLopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuanLyHocSinhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caiDatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSachLopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSachToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NhapDiemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TheoDoiHocTapToolStripMenuItem;
    }
}