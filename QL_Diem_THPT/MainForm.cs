﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class MainForm : Form
    {
        Session session;
        public MainForm(Session session)
        {
            InitializeComponent();
            this.session = session;
        }


        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (session.acc.loaiTaiKhoan == 0)
            {
                fileToolStripMenuItem.DropDownItems.Remove(thongTinToolStripMenuItem);
                menuStrip1.Items.Remove(QuanLyHocSinhToolStripMenuItem);
            }
            if (session.acc.loaiTaiKhoan != 0)
                menuStrip1.Items.Remove(quanTriVienToolStripMenuItem);
        }

        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name = ((ToolStripMenuItem)sender).Text;
            foreach (Form f in MdiChildren)
                if (f.Name == name)
                {
                    f.Activate();
                    return;
                }
            Form form;
            switch(name)
            {
                case "Thông Tin Cá Nhân":
                    form =(Form)new AccInfomation(session.acc);
                    break;
                case "Học Sinh":
                    form = (Form)new Form_HocSinh(session);
                    break;
                case "Giáo Viên":
                    form = (Form)new Form_GiaoVien(session);
                    break;
                case "Danh Sách Lớp":
                    form = (Form)new Form_Lop(session);
                    break;
                case "Quản Lý Lớp":
                    form = (Form)new Form_QL_Lop(session);
                    break;
                case "Bộ Môn":
                    form = (Form)new Form_BoMon(session);
                    break;
                case "Nhập Điểm":
                    form = (Form)new Form_NhapDiem(session);
                    break;
                case "Theo Dõi Học Tập":
                    form = (Form)new Form_BangDiem(session);
                    break;
                default:
                    form = (Form)new AccSecurity(session.acc);
                    break;
            }
            form.Name = name;
            form.MdiParent = this;
            form.Show();
        }
        private void dangXuatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.SetShow("Login");
            session.Logout();
            Close();
        }
        private void thoatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (session.acc.isLogin == true)
            {
                session.Logout();
                session.Close();
            }
        }
    }
}
