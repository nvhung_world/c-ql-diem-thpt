﻿namespace QL_Diem
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.matKhau = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DangNhapbutton = new System.Windows.Forms.Button();
            this.Messenge = new System.Windows.Forms.Label();
            this.QuenMatKhau = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.taiKhoan = new System.Windows.Forms.ComboBox();
            this.checkSave = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tài Khoản";
            // 
            // matKhau
            // 
            this.matKhau.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matKhau.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matKhau.Location = new System.Drawing.Point(75, 38);
            this.matKhau.MaxLength = 30;
            this.matKhau.Name = "matKhau";
            this.matKhau.PasswordChar = '*';
            this.matKhau.Size = new System.Drawing.Size(140, 21);
            this.matKhau.TabIndex = 3;
            this.matKhau.Text = "admin";
            this.matKhau.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPess);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mật khẩu";
            // 
            // DangNhapbutton
            // 
            this.DangNhapbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.DangNhapbutton.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DangNhapbutton.Location = new System.Drawing.Point(221, 3);
            this.DangNhapbutton.Name = "DangNhapbutton";
            this.DangNhapbutton.Size = new System.Drawing.Size(74, 79);
            this.DangNhapbutton.TabIndex = 4;
            this.DangNhapbutton.Text = "Đăng Nhập";
            this.DangNhapbutton.UseVisualStyleBackColor = false;
            this.DangNhapbutton.Click += new System.EventHandler(this.DangNhapButton_Click);
            // 
            // Messenge
            // 
            this.Messenge.AutoSize = true;
            this.Messenge.BackColor = System.Drawing.Color.Transparent;
            this.Messenge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Messenge.Location = new System.Drawing.Point(12, 142);
            this.Messenge.Name = "Messenge";
            this.Messenge.Size = new System.Drawing.Size(143, 13);
            this.Messenge.TabIndex = 5;
            this.Messenge.Text = "Đang kết nối cơ sở dữ liệu ...";
            // 
            // QuenMatKhau
            // 
            this.QuenMatKhau.AutoSize = true;
            this.QuenMatKhau.Cursor = System.Windows.Forms.Cursors.Hand;
            this.QuenMatKhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuenMatKhau.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuenMatKhau.Location = new System.Drawing.Point(13, 66);
            this.QuenMatKhau.Name = "QuenMatKhau";
            this.QuenMatKhau.Size = new System.Drawing.Size(93, 13);
            this.QuenMatKhau.TabIndex = 6;
            this.QuenMatKhau.Text = "Quên mật khẩu";
            this.QuenMatKhau.Click += new System.EventHandler(this.QuenMatKhau_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.taiKhoan);
            this.mainPanel.Controls.Add(this.checkSave);
            this.mainPanel.Controls.Add(this.QuenMatKhau);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Controls.Add(this.label2);
            this.mainPanel.Controls.Add(this.DangNhapbutton);
            this.mainPanel.Controls.Add(this.matKhau);
            this.mainPanel.Enabled = false;
            this.mainPanel.Location = new System.Drawing.Point(7, 158);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(300, 91);
            this.mainPanel.TabIndex = 7;
            // 
            // taiKhoan
            // 
            this.taiKhoan.FormattingEnabled = true;
            this.taiKhoan.Location = new System.Drawing.Point(75, 11);
            this.taiKhoan.MaxLength = 10;
            this.taiKhoan.Name = "taiKhoan";
            this.taiKhoan.Size = new System.Drawing.Size(140, 21);
            this.taiKhoan.TabIndex = 8;
            this.taiKhoan.Text = "admin";
            this.taiKhoan.SelectedIndexChanged += new System.EventHandler(this.taiKhoan_SelectedIndexChanged);
            // 
            // checkSave
            // 
            this.checkSave.AutoSize = true;
            this.checkSave.Location = new System.Drawing.Point(124, 65);
            this.checkSave.Name = "checkSave";
            this.checkSave.Size = new System.Drawing.Size(91, 17);
            this.checkSave.TabIndex = 7;
            this.checkSave.Text = "Lưu tài khoản";
            this.checkSave.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(314, 155);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 261);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.Messenge);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng Nhập";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Login_FormClosed);
            this.Shown += new System.EventHandler(this.Login_Shown);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox matKhau;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button DangNhapbutton;
        private System.Windows.Forms.Label Messenge;
        private System.Windows.Forms.Label QuenMatKhau;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.CheckBox checkSave;
        private System.Windows.Forms.ComboBox taiKhoan;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

