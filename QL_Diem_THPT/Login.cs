﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QL_Diem;
using qldBLL;
namespace QL_Diem
{
    public partial class Login : Form
    {
        Session session;
        public Login(Session session)
        {
            InitializeComponent();
            mainPanel.Enabled = false;
            this.session = session;
        }

        private void Login_Shown(object sender, EventArgs e)
        {
            // thử kết nối cơ sở dữ liệu
            bool b = true;
            while (b)
            {
                switch (session.Open())
                {
                    case 1:
                        Messenge.Text = "Lỗi: Không có thông tin Server";
                        b = false;
                        break;
                    case 2:
                        Messenge.Text = "Lỗi: Không thể kết nối Server";
                        DialogResult dl = MessageBox.Show("Thử kết nối lại",
                            "Lỗi: Không thể kết nối Server",
                            MessageBoxButtons.RetryCancel,
                            MessageBoxIcon.Question);
                        if (dl != System.Windows.Forms.DialogResult.Retry)
                        {
                            b = false;
                            Close();
                        }
                        break;
                    default:
                        mainPanel.Enabled = true;
                        Messenge.Text = "";
                        mainPanel.Enabled = true;
                        b = false;
                        break;
                }
            }
            //  Lạp tài khoản được lưu
            AccSeve.Read();
            foreach (Dictionary<string, string> acc in AccSeve.list)
                taiKhoan.Items.Add(acc["acc"]);
            if(AccSeve.list.Count > 0)
                taiKhoan.Text = AccSeve.list[0]["acc"];
            checkSave.Checked = AccSeve.SaveAcc;
        }

        private void DangNhapButton_Click(object sender, EventArgs e)
        {
            DangNhapEvent();
        }
        private void DangNhapEvent()
        {
            if (taiKhoan.Text != "" && matKhau.Text != "")
                switch (session.Login(taiKhoan.Text, matKhau.Text))
                {
                    case 1:
                        Messenge.Text = "Lỗi: Tài Khoản không tồn tại hoặc sai mật khẩu";
                        break;
                    case 2:
                        Messenge.Text = "Lỗi: sự cố với cơ sở dữ liệu";
                        break;
                    default:
                        if (session.acc.loaiTaiKhoan > -1)
                        {
                            Messenge.Text = "Đăng nhập thành công";
                            Program.SetShow("MainForm");
                            //Luu tai khoan vua dang nhap
                            AccSeve.SaveAcc = checkSave.Checked;
                            if (checkSave.Checked)
                                AccSeve.Add(taiKhoan.Text, matKhau.Text);
                            else
                                AccSeve.Save();
                            Close();
                        }
                        else
                        {
                            Messenge.Text = "Lỗi: Tài khoản hết hạn sử dụng";
                        }
                        break;
                }
        }
        private void KeyPess(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
                DangNhapEvent();
        }

        private void taiKhoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                matKhau.Text = AccSeve.list[taiKhoan.SelectedIndex]["pass"];
            }
            catch(Exception)
            { }
        }

        private void QuenMatKhau_Click(object sender, EventArgs e)
        {
            (new QuenMatKhau()).ShowDialog();
        }
        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (session.acc.isLogin == false)
            {
                session.Logout();
                session.Close();
            }
        }

    }
}
