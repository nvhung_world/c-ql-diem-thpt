﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class Form_NhapDiem : Form
    {
        int hocKi
        {
            get
            {
                int Month = DateTime.Now.Month;
                if (Month < 6)
                    return 2;   // Học kì 2
                else if (Month > 8)
                    return 1;   // Học kì 2
                else
                    return -1;   // Nghỉ Hè
            }
        }
        int namHoc
        {
            get
            {
                int Month = DateTime.Now.Month;
                if (Month > 8)
                    return DateTime.Now.Year;
                else if (Month < 6)
                    return DateTime.Now.Year - 1;
                else
                    return -1;
                    
            }
        }
        string MaGV;
        NhapDiem Data;
        string tenlop;
        int khoi;
        public Form_NhapDiem(Session session)
        {
            InitializeComponent();
            if (namHoc > 0)
            {
                Text = "Nhập điểm - Học kì " + hocKi + " - Năm học " + namHoc + " - " + (namHoc + 1);
                MaGV = session.acc.maGV;
                Data = new NhapDiem(session,hocKi,namHoc);
            }
            else
                Text = "Không nằm trong thời gian học tập";
        }

        private void Form_Shown(object sender, EventArgs e)
        {
            if (namHoc < 0)
                return;
            string s = Data.Init();
            if (s == "")
            {
                foreach (int Khoi in Data.List.Keys)
                    foreach (string Lop in Data.List[Khoi].Keys)
                        treeView1.Nodes[0].Nodes[Khoi].Nodes.Add(Lop);
            }
            else
                qL_Log1.Log += s + "\n";
        }
        /// <summary>
        /// Data GridView
        /// </summary>
        private void data_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            string message = "Lỗi định dạng dữ liệu: dòng " + e.RowIndex + " cột " + e.ColumnIndex;
            message += "\n" + e.Exception.Message;
            MessageBox.Show(message, "Dữ liệu không hợp lệ");
        }
        private void data_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CN_button.Enabled = true;
        }
        private void tree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Parent == null)
                return;
            tenlop = e.Node.Text;
            if (e.Node.Parent.Text == "Khối 10")
                khoi = 0;
            else if (e.Node.Parent.Text == "Khối 11")
                khoi = 1;
            else if (e.Node.Parent.Text == "Khối 12")
                khoi = 2;
            List<string> s = Data.ReadTable(khoi, tenlop);
            if (s.Count > 0)
            {
                if(s[0] == "")
                {
                    dataGridView1.DataSource = Data.table;
                    CN_button.Enabled = false;
                    label1.Text = "Lớp: " + tenlop;
                    label2.Text = "Năm Học: " + namHoc;
                    label3.Text = "Giáo viên chủ nhiệm: " + s[1];
                    label4.Text = "Loại hình đào tạo: " + s[2];
                    qL_Log1.Log += "--------------- Đọc dữ liệu thành công\n";
                }
                else
                    qL_Log1.Log += "lỗi: " + s[0] + "\n";
            }
        }

        private void CN_button_Click(object sender, EventArgs e)
        {
            string s = Data.Update();
            if (s == "")
            {
                CN_button.Enabled = false;
                Data.ReadTable(khoi, tenlop);
                qL_Log1.Log += "--------------- Cập nhật dữ liệu thành công\n";
            }else
                qL_Log1.Log += "lỗi: " + s + "\n";
        }

        private void Form_CLosing(object sender, FormClosingEventArgs e)
        {
            if (CN_button.Enabled)
            {
                object o = MessageBox.Show("Các thay đổi trên dữ liệu chưa được cập  nhật! \n Bạn có muốn lưu các thay đổi !", "Cảnh Báo !", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if ((DialogResult)o == DialogResult.Yes)
                    Data.Update();
            }
        }

    }
}
