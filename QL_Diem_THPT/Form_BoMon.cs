﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class Form_BoMon : Form
    {
        //bllAdapter table;
        Table data;
        public Form_BoMon(Session session)
        {
            InitializeComponent();
            data = new Table(session);
            data.select.Add(new QuerySelect());
            data.select[0].AddString(EnumQuery.select, "*");
            data.select[0].AddString(EnumQuery.from, "BoMon");
            data.SetCommand(Statement.select);
        }

        private void SetupTable()       //Cài đặt điều kiện dữ liệu cho bảng
        {
            data.table.Columns[0].AllowDBNull = false;
            data.table.Columns[0].MaxLength = 20;

            data.table.Columns[1].AllowDBNull = false;
            data.table.Columns[1].DefaultValue = 1;

            data.table.Columns[2].AllowDBNull = false;
            data.table.Columns[2].MaxLength = 10;

            data.table.Columns[3].MaxLength = 10;
        }
        /// <summary>
        /// Form
        /// </summary>
        private void Form_Shown(object sender, EventArgs e)
        {
            string s = data.ReadTable("BoMon");
            if (s == "")
            {
                dataGridView1.DataSource = data.table;
                SetupTable();
                button1.Enabled = false;
            }
            else
                qL_Log1.Log += "Lỗi :" + s + "\n";
        }
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (button1.Enabled)
            {
                object o = MessageBox.Show("Các thay đổi trên dữ liệu chưa được cập  nhật! \n Bạn có muốn lưu các thay đổi !", "Cảnh Báo !", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if ((DialogResult)o == DialogResult.Yes)
                    data.Update();
            }
        }
        /// <summary>
        /// Control
        /// </summary>
        private void CapNhat_Click(object sender, EventArgs e)
        {
            string s =  data.Update();
            if(s=="")
            {
                qL_Log1.Log += "---------- Cập nhật dữ liệu thành công\n";
                Form_Shown(this, null);
                button1.Enabled = false;
            }
            else
                qL_Log1.Log += "Lỗi :" + s + "\n";
        }
        /// <summary>
        /// Data GridView
        /// </summary>
        private void data_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            int count = dataGridView1.SelectedRows.Count;
            object o = MessageBox.Show("Bạn có chắc muốn xóa " + count + " hàng! ", "Cân Nhắc", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if ((DialogResult)o == DialogResult.No)
                e.Cancel = true;
        }
        private void data_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            button1.Enabled = true;
        }
        private void data_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            string message = "Lỗi định dạng dữ liệu: dòng " + e.RowIndex + " cột " + e.ColumnIndex ;
            message += "\n" + e.Exception.Message;
            MessageBox.Show(message, "Dữ liệu không hợp lệ");
        }
        private void data_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            button1.Enabled = true;
        }
    }
}
