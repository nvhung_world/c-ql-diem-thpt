﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_Diem
{
    static class Program
    {
        static Dictionary<string,Form> list;
        static Dictionary<string,bool> show;
        static qldBLL.Session session;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            session = new qldBLL.Session();
            list = new Dictionary<string,Form>();
            show = new Dictionary<string, bool>();
            list.Add("Login", new Login(session));
            show.Add("Login", true);
            list.Add("MainForm", new MainForm(session));
            show.Add("MainForm", false);
            while(true)
            {
                bool b = false;
                foreach (string k in list.Keys)
                {
                    if (show[k])
                    {
                        b = true;
                        Application.Run(list[k]);
                        show[k] = false;
                        break;
                    }
                }
                if (b == false)
                    break;
            }
        }

        public static void SetShow(string form)
        {
            bool b;
            if (show.TryGetValue(form, out b))
            {
                show[form] = true;
                switch (form)
                {
                    case "Login":
                        list[form] = new Login(session);
                        break;
                    case "MainForm":
                        list[form] = new MainForm(session);
                        break;
                }
            }
            else
                MessageBox.Show("Lỗi: Không Tìm thấy Form " + form);
        }
    }
}
