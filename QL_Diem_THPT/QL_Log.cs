﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QL_Diem
{
    public partial class QL_Log : UserControl
    {
        string name;
        public string fromName
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        }
        public string Log
        {
            set
            {
                richTextBox1.Text = value;
                if (show != null && !show.IsDisposed)
                    show.richTextBox1.Text = value;
            }
            get
            {
                return richTextBox1.Text;
            }
        }
        ShowLog show;
        public QL_Log()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (show == null || show.IsDisposed)
            {
                show = new ShowLog(fromName);
                show.richTextBox1.Text = richTextBox1.Text;
                show.Show();
            }
        }
    }
}
