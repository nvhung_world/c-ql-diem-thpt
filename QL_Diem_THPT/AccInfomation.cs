﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class AccInfomation : Form
    {
        Acc acc;
        public AccInfomation(Acc acc)
        {
            InitializeComponent();
            this.acc = acc;
            acc.GetInfo();
            ReadInfo();
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            string s = acc.DoiThongTinGiaoVien(hoDem: HoDem.Text,
                ten: Ten.Text,
                ngaySinh: ngaySinh.Text,
                gioiTinh: (Nam.Checked ?  1 : 0),
                diaChi: DiaChi.Text,
                email: Email.Text,
                sdt: Sdt.Text
                );
            if (s != "")
                label11.Text = s;
            else
                button1.Enabled = false;
        }

        private void GT_Click(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Text == "Nam")
            {
                Nu.Checked = false;
            }
            else
            {
                Nam.Checked = false;
            }
            button1.Enabled = true;
        }

        private void Text_Changed(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }

        private void ReadInfo()
        {
            MaGV.Text = "Ma Giao Vien: " + acc.maGV;
            IDM.Text = "Bộ Môn: " + acc.BoMon;
            HoDem.Text = acc.hoDem;
            Ten.Text = acc.ten;
            SoCMt.Text = acc.soCMT;
            ngaySinh.Text = acc.ngaySinh;
            if (acc.gioiTinh == 1)
            {
                Nam.Checked = true;
                Nu.Checked = false;
            }
            else
            {
                Nam.Checked = false;
                Nu.Checked = true;
            }
            NVT.Text = acc.ngayVaoTruong;
            NRT.Text = acc.ngayRoiTruong;
            DiaChi.Text = acc.diaChi;
            Email.Text = acc.email;
            Sdt.Text = acc.sdt;
        }
    }
}
