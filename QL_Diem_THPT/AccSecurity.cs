﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using qldBLL;
namespace QL_Diem
{
    public partial class AccSecurity : Form
    {
        Acc acc;
        public AccSecurity(Acc acc)
        {
            InitializeComponent();
            this.acc = acc;
            if (acc != null)
            {
                switch(acc.loaiTaiKhoan)
                {
                    case 0: 
                        label1.Text = "Loại tài khoản: Quản trị viên";
                        break;
                    case 1:
                        label1.Text = "Loại tài khoản: Giáo viên";
                        break;
                }
                label2.Text = "Tên đăng nhập: " + acc.maGV;
            }
        }

        private void CN_MatKhau(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (textBox1.Text != acc.matKhau)
            {
                MSGlabel.Text = "Sai mật khẩu hiện tại";
                errorProvider1.SetError(textBox1, "Sai mật khẩu hiện tại");
                return;
            }
            else
            if(textBox2.Text.Length < 6)
            {
                MSGlabel.Text = "Mật khẩu phải lớn hơn 6 kí tự";
                errorProvider1.SetError(textBox2, "Mật khẩu phải lớn hơn 6 kí tự");
                return;
            }
            if (textBox2.Text != textBox3.Text)
            {
                MSGlabel.Text = "Mật khẩu không trùn khớp";
                errorProvider1.SetError(textBox3, "Mật khẩu không trùn khớp");
                return;
            }
            acc.DoiThongTinDangNhap(textBox2.Text);
        }

        private void CN_MatKhau_2(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (textBox6.Text != acc.matKhau)
            {
                MSGlabel.Text = "Sai mật khẩu hiện tại";
                errorProvider1.SetError(textBox6, "Sai mật khẩu hiện tại");
                return;
            }
            else
                if (textBox5.Text.Length < 6)
                {
                    MSGlabel.Text = "Mật khẩu phải lớn hơn 6 kí tự";
                    errorProvider1.SetError(textBox5, "Mật khẩu phải lớn hơn 6 kí tự");
                    return;
                }
            if (textBox4.Text != textBox5.Text)
            {
                MSGlabel.Text = "Mật khẩu không trùn khớp";
                errorProvider1.SetError(textBox4, "Mật khẩu không trùn khớp");
                return;
            }
            acc.DoiThongTinDangNhap(matKhauCap_2: textBox5.Text);
        }

        private void CN_CauHoi(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (textBox9.Text != acc.matKhau)
            {
                MSGlabel.Text = "Sai mật khẩu hiện tại";
                errorProvider1.SetError(textBox9, "Sai mật khẩu hiện tại");
                return;
            }
            else
                if (comboBox1.Text.Length < 6)
                {
                    MSGlabel.Text = "Câu hỏi phải lớn hơn 6 kí tự";
                    errorProvider1.SetError(comboBox1, "Câu hỏi phải lớn hơn 6 kí tự");
                    return;
                }
            if (textBox7.Text.Length < 4)
            {
                MSGlabel.Text = "Câu trả phải lớn hơn 4 kí tự";
                errorProvider1.SetError(textBox7, "Câu trả phải lớn hơn 4 kí tự");
                return;
            }
            acc.DoiThongTinDangNhap(cauHoi: comboBox1.Text, traLoi: textBox7.Text);
        }
    }
}
