﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.Data;
namespace qldDAL
{
    public class Connect
    {
        public bool isConnect;
        public SqlConnection conn;
        public Connect()
        {
            isConnect = false;
        }

        public int Open()
        {
            // Lấy thông tin kết nối
            StreamReader cnS;
            try
            {
                cnS = new StreamReader(File.OpenRead(".\\connectString.cnS"));
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                return 1; //Không có thông tin Server
            }
            // Đọc chuỗi kết nối
            string connectString = cnS.ReadLine();
            cnS.Close();


            // Mở kết nối
            conn = new SqlConnection(connectString);
            try
            {
                conn.Open();
                isConnect = true;
            }
            catch (Exception)
            {
                return 2; //Không thể kết nối Server
            }
            return 0;   //Kết nối thành công
        }

        public void Close()
        {
            if (isConnect)
            {
                try
                {
                    conn.Close();

                }
                catch(Exception)
                {

                }
                finally
                {
                    isConnect = false;
                }
            }
        }

    }
}
