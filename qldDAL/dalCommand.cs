﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace qldDAL
{
    public class dalCommand
    {
        public string commandText;
        public CommandType type;
        public List<SqlParameter> parameter;
        public dalCommand()
        {
            type = CommandType.Text;
            parameter = new List<SqlParameter>();
            commandText = "";
        }
        public void AddValue(SqlParameter value)
        {
            parameter.Add(value);
        }
        public void ClearValue()
        {
            parameter.Clear();
        }
    }
}
