﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace qldDAL
{
    public class Adapter
    {
        SqlDataAdapter da;
        public DataTable table;
        Connect connect;
        public dalCommand select, insert, update, delete;

        public Adapter(Connect connect)
        {
            da = new SqlDataAdapter();
            this.connect = connect;
        }
        public DataTable GetTable(string tablename)
        {
            if (table != null)
            {
                table.Clear();
                table.TableName = tablename;
            }
            else
                table = new DataTable(tablename);
            // Lếu Command đã tồn tại thì xóa hết Parameter của nó
            if (da.SelectCommand != null) 
                da.SelectCommand.Parameters.Clear();
            // Tạo Command mới
            SqlCommand command = new SqlCommand(select.commandText, connect.conn);
            command.CommandType = select.type;
            command.Parameters.AddRange(select.parameter.ToArray<SqlParameter>());
            da.SelectCommand = command;
            //nấy dữ liệu về
            da.Fill(table);
            SqlCommandBuilder scb = new SqlCommandBuilder(da);
            return table;
        }
        public void Update()
        {
            // Lếu Command đã tồn tại thì xóa hết Parameter của nó
            if (da.InsertCommand != null)
                da.InsertCommand.Parameters.Clear();
            if (da.UpdateCommand != null)
                da.UpdateCommand.Parameters.Clear();
            if (da.DeleteCommand != null)
                da.DeleteCommand.Parameters.Clear();
            // Tạo Command mới
            if (insert != null)
            {
                SqlCommand ins = new SqlCommand(insert.commandText, connect.conn);
                ins.CommandType = insert.type;
                ins.Parameters.AddRange(insert.parameter.ToArray<SqlParameter>());
                da.InsertCommand = ins;
            }
            if (update != null)
            {
                SqlCommand upd = new SqlCommand(update.commandText, connect.conn);
                upd.CommandType = update.type;
                upd.Parameters.AddRange(update.parameter.ToArray<SqlParameter>());
                da.UpdateCommand = upd;
            }
            if (delete != null)
            {
                SqlCommand del = new SqlCommand(delete.commandText, connect.conn);
                del.CommandType = delete.type;
                del.Parameters.AddRange(delete.parameter.ToArray<SqlParameter>());
                da.DeleteCommand = del;
            }

            // cập nhật dữ liệu
            da.Update(table);
        }

        public void SetCommand(CommandName commandName, string commandText, CommandType type)
        {
            switch (commandName)
            {
                case CommandName.select:
                    if(select == null)
                        select = new dalCommand();
                    select.commandText = commandText;
                    select.type = type;
                    break;
                case CommandName.insert:
                    if (insert == null)
                        insert = new dalCommand();
                    insert.commandText = commandText;
                    insert.type = type;
                    break;
                case CommandName.update:
                    if (update == null)
                        update = new dalCommand();
                    update.commandText = commandText;
                    update.type = type;
                    break;
                case CommandName.delete:
                    if (delete == null)
                        delete = new dalCommand();
                    delete.commandText = commandText;
                    delete.type = type;
                    break;
            }
        }
        public void SetCommand(CommandName commandName, string commandText)
        {
            switch (commandName)
            {
                case CommandName.select:
                    if (select == null)
                        select = new dalCommand();
                    select.commandText = commandText;
                    break;
                case CommandName.insert:
                    if (insert == null)
                        insert = new dalCommand();
                    insert.commandText = commandText;
                    break;
                case CommandName.update:
                    if (update == null)
                        update = new dalCommand();
                    update.commandText = commandText;
                    break;
                case CommandName.delete:
                    if (delete == null)
                        delete = new dalCommand();
                    delete.commandText = commandText;
                    break;
            }
        }

        public void AddValue(CommandName commandType, SqlParameter value)
        {
            dalCommand comm;
            switch (commandType)
            {
                case CommandName.select:
                    comm = select;
                    break;
                case CommandName.insert:
                    comm = insert;
                    break;
                case CommandName.update:
                    comm = update;
                    break;
                default:
                    comm = delete;
                    break;
            }
            if (comm == null)
                throw new Exception("Command chua khoi tao");
            comm.AddValue(value);
        }
        public void ClearValue(CommandName commandName)
        {
            dalCommand comm;
            switch (commandName)
            {
                case CommandName.select:
                    comm = select;
                    break;
                case CommandName.insert:
                    comm = insert;
                    break;
                case CommandName.update:
                    comm = update;
                    break;
                default:
                    comm = delete;
                    break;
            }
            if (comm != null)
                comm.ClearValue();
        }
    }
}
